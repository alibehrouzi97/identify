using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Data;
using Identity.Models.Entities.ClientRedirectUriEntity;
using Identity.Models.Entities.ClientScopeEntity;
using Identity.Models.RepoParams.ClientRedirectUriEntity;
using Identity.Models.RepoParams.ClientScopeEntity;
using Identity.Models.RepoResults.ClientRedirectUri;
using Identity.Models.RepoResults.ClientScope;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Identity.Repository
{
    public interface IClientScopeRepository
    {
        Task DeleteClientScopeAsync(int id);
        Task<CreateClientScopeRepoResult> GetOneClientScopeAsync(int? id);
        Task<List<CreateClientScopeRepoResult>> GetAllClientScopesAsync();

        Task<CreateClientScopeRepoResult> UpdateClientScopeAsync(
            UpdateClientScopeRepoParam updateClientScopeRepoParam, int id);

        Task<CreateClientScopeRepoResult> CreateClientScopeAsync(
            CreateClientScopeRepoParam createClientScopeRepoParam);
        
    }
    public class ClientScopeRepository : IClientScopeRepository
    {
        
        private readonly ILogger<IClientScopeRepository> _logger;
        
        private readonly MvcIdentityDbContext _context;
        
        public ClientScopeRepository(ILogger<IClientScopeRepository> logger, MvcIdentityDbContext context)
        {
            _context = context;
            _logger = logger;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        
        public async Task<CreateClientScopeRepoResult> CreateClientScopeAsync(
            CreateClientScopeRepoParam createClientScopeRepoParam)
        {
            
            if (createClientScopeRepoParam == null)
            {
                _logger.LogError("createClientScopeRepoParam is null ,CreateClientScopeAsync in ClientScopeRepository");
                throw new Exception("createClientScopeRepoParam is null ,CreateClientScopeAsync in ClientScopeRepository");
            }

            var clientScope = new ClientScope
            {
                Scope = createClientScopeRepoParam.Scope,
                ClientId = createClientScopeRepoParam.ClientId,
            };
            
            await _context.ClientScopes.AddAsync(clientScope);
            await _context.SaveChangesAsync();
            
            return new CreateClientScopeRepoResult(clientScope);
        }
        
        public async Task<CreateClientScopeRepoResult> UpdateClientScopeAsync(
            UpdateClientScopeRepoParam updateClientScopeRepoParam,int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, UpdateClientScopeAsync ClientScopeRepository");
                throw new Exception("id is null, UpdateClientScopeAsync ClientScopeRepository");

            }
            
            if (updateClientScopeRepoParam == null)
            {
                _logger.LogError("updateClientScopeRepoParam is null ,UpdateClientScopeAsync in ClientScopeRepository");
                throw new Exception("updateClientScopeRepoParam is null ,UpdateClientScopeAsync in ClientScopeRepository");
            }

            var clientScope = await _context.ClientScopes.FirstOrDefaultAsync(c => c.Id == id);
            
            if (clientScope == null)
            {
                
                _logger.LogError("clientScope not found in DB,UpdateClientScopeAsync ClientScopeRepository ");
                throw new Exception("clientScope not found in DB,UpdateClientScopeAsync ClientScopeRepository");

            }
            
            var scope = new ClientScope
            {
                Id =id,
                ClientId = updateClientScopeRepoParam.ClientId,
                Scope = updateClientScopeRepoParam.Scope
            };
            
            _context.ClientScopes.Update(scope);
            await _context.SaveChangesAsync();
            
            return new CreateClientScopeRepoResult(scope);
            
        }
        public async Task<List<CreateClientScopeRepoResult>> GetAllClientScopesAsync()
        {
            
            var clientScopes = await _context.ClientScopes.ToListAsync();
            
            if (clientScopes == null)
            {
                _logger.LogError("no clientScope in DB,GetAllClientScopesAsync in ClientScopeRepository");
                throw new Exception("no clientScope in DB,GetAllClientScopesAsync in ClientScopeRepository");

            }

            var clientScopeRepoResults = new List<CreateClientScopeRepoResult>();
            
            foreach (var clientScope in clientScopes)
            {
                clientScopeRepoResults.Add(new CreateClientScopeRepoResult(clientScope));
            }

            return clientScopeRepoResults;
            
        }
        public async Task<CreateClientScopeRepoResult> GetOneClientScopeAsync(int? id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, GetOneClientScopeAsync ClientScopeRepository");
                throw new Exception("id is null, GetOneClientScopeAsync ClientScopeRepository");
            }
            
            var clientScope = await _context.ClientScopes.FirstOrDefaultAsync(c => c.Id == id);

            if (clientScope == null)
            {
                _logger.LogError("clientScope not found in DB,GetOneClientScopeAsync ClientScopeRepository");
                throw new Exception("clientScope not found in DB,GetOneClientScopeAsync ClientScopeRepository");
            }
            
            return new CreateClientScopeRepoResult(clientScope);

        }
        public async Task DeleteClientScopeAsync(int id)
        {
            
            if (id.ToString()==null)
            {
                _logger.LogError("id is null,DeleteClientScopeAsync ClientScopeRepository");
                throw new Exception("Id is null");
            }

            try
            {
                
                var scope = await _context.ClientScopes.FirstOrDefaultAsync(clientScope => clientScope.Id == id);
                
                if (scope==null)
                {
                    _logger.LogError("scope is null,DeleteClientScopeAsync ClientScopeRepository");
                    throw new Exception("scope is null,DeleteClientScopeAsync ClientScopeRepository");
                }
                
                _context.ClientScopes.Remove(scope);
                await _context.SaveChangesAsync();
                
            }
            
            catch (Exception)
            {
                _logger.LogError("clientScope  not removed,DeleteClientScopeAsync ClientScopeRepository");
                throw new Exception("clientScope didnt remove");
            }
            
        }
    }
}