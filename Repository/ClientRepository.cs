using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Identity.Data;
using Identity.Models.Entities.ClientClaimEntity;
using Identity.Models.Entities.ClientCorsOriginEntity;
using Identity.Models.Entities.ClientEntity;
using Identity.Models.Entities.ClientGrantTypeEntity;
using Identity.Models.Entities.ClientPostLogoutRedirectUriEntity;
using Identity.Models.Entities.ClientRedirectUriEntity;
using Identity.Models.Entities.ClientScopeEntity;
using Identity.Models.RepoParams.Client;
using Identity.Models.RepoResults.Client;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Identity.Repository
{
    public interface IClientRepository
    {
        Task<CreateClientRepoResult> CreateClientAsync(CreateClientRepoParam createClientRepoParam);
        Task<List<CreateClientRepoResult>> GetAllClientsAsync();
        Task<CreateClientRepoResult> GetOneClientAsync(int? id);
        Task DeleteClientAsync(int id);
        Task<CreateClientRepoResult> UpdateClientAsync(UpdateClientRepoParam updateClientRepoParam, int id);
    }
    public class ClientRepository : IClientRepository
    {
        private readonly ILogger<IClientRepository> _logger;
        
        private readonly MvcIdentityDbContext _context;

        public ClientRepository(ILogger<IClientRepository> logger, MvcIdentityDbContext context)
        {
            _context = context;
            _logger = logger;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public async Task<CreateClientRepoResult> CreateClientAsync(CreateClientRepoParam createClientRepoParam)
        {
            
            if (createClientRepoParam == null)
            {
                _logger.LogError("CreateClientRepoParam is null ,CreateClientAsync in ClientRepository");
                throw new Exception("CreateClientRepoParam is null ,CreateClientAsync in ClientRepository");

            }
            
            var client = new Client
            {
                ClientName = createClientRepoParam.ClientName,
                Description = createClientRepoParam.Description,
                RequirePkce = createClientRepoParam.RequirePkce,
                AllowOfflineAccess = createClientRepoParam.AllowOfflineAccess,
                AlwaysSendClientClaims = createClientRepoParam.AlwaysSendClientClaims,
                LastAccessed = createClientRepoParam.LastAccessed
            };

            if (createClientRepoParam.AllowedGrantTypes!=null)
            {
                
                var clientGrantTypes= new List<ClientGrantType>();
                
                foreach (var grantType in createClientRepoParam.AllowedGrantTypes)
                {
                    
                    clientGrantTypes.Add(new ClientGrantType
                    {
                        GrantType = grantType,
                    });
                }
                
                client.AllowedGrantTypes=clientGrantTypes; 
            }
            
            if (createClientRepoParam.RedirectUris!=null)
            {
                
                var clientRedirectUris= new List<ClientRedirectUri>();
                
                foreach (var redirectUri in createClientRepoParam.RedirectUris)
                {
                    
                    clientRedirectUris.Add(new ClientRedirectUri
                    {
                        RedirectUri = redirectUri,
                    });
                }
                
                client.RedirectUris = clientRedirectUris;
            }

            if (createClientRepoParam.PostLogoutRedirectUris!=null)
            {
                
                var clientPostLogoutRedirectUriEntity = new List<ClientPostLogoutRedirectUri>();
                
                foreach (var logoutRedirectUri in createClientRepoParam.PostLogoutRedirectUris)
                {
                    clientPostLogoutRedirectUriEntity.Add(new ClientPostLogoutRedirectUri
                    {
                        PostLogoutRedirectUri = logoutRedirectUri,
                    });
                }
                
                client.PostLogoutRedirectUris = clientPostLogoutRedirectUriEntity;
                
            }

            if (createClientRepoParam.AllowedScopes!=null)
            {
                
                var clientScope=new List<ClientScope>();
                
                foreach (var scope in createClientRepoParam.AllowedScopes)
                {
                    
                    clientScope.Add(new ClientScope
                    {
                        Scope = scope,
                    });
                }
                
                client.AllowedScopes = clientScope;
            }


            if (createClientRepoParam.Claims!=null)
            {
                
                var clientClaim=new List<ClientClaim>();
                
                foreach (var claim in createClientRepoParam.Claims)
                {
                    
                    clientClaim.Add(new ClientClaim
                    {
                        Type = claim,
                        Value = claim
                    });
                }
                
                client.Claims = clientClaim;
            }

            if (createClientRepoParam.AllowedCorsOrigins!=null)
            {
                
                var clientCorsOrigin=new List<ClientCorsOrigin>();
                
                foreach (var cors in createClientRepoParam.AllowedCorsOrigins)
                {
                    clientCorsOrigin.Add(new ClientCorsOrigin
                    {
                        Origin = cors
                    });
                }
                
                client.AllowedCorsOrigins = clientCorsOrigin;
            }            

            
            await _context.Client.AddAsync(client);
            await _context.SaveChangesAsync();
            
            return new CreateClientRepoResult(client);
            
        }

        public async Task<CreateClientRepoResult> UpdateClientAsync(UpdateClientRepoParam updateClientRepoParam,int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, UpdateClientAsync ClientRepository");
                throw new Exception("id is null, UpdateClientAsync ClientRepository");

            }
            
            if (updateClientRepoParam == null)
            {
                _logger.LogError("updateClientRepoParam is null");
                throw new Exception("updateClientRepoParam null");

            }

            var client = await _context.Client
                .Include(c=> c.Claims)
                .Include(c=>c.AllowedScopes)
                .Include(c=>c.RedirectUris)
                .Include(c=> c.AllowedCorsOrigins)
                .Include(c=>c.AllowedGrantTypes)
                .Include(c=>c.PostLogoutRedirectUris)
                .FirstOrDefaultAsync(c => c.Id == id);
            
            if (client == null)
            {
                _logger.LogError("client not found in DB,UpdateClientAsync ClientRepository ");
                throw new Exception("client not found in DB,UpdateClientAsync ClientRepository");
            }
            
            _context.ClientClaims.RemoveRange( await _context.ClientClaims.Where(c => c.ClientId == client.Id).ToListAsync());
            
            _context.ClientRedirectUris.RemoveRange(await _context.ClientRedirectUris.Where(c => c.ClientId == client.Id).ToListAsync());
            
            _context.ClientPostLogoutRedirectUris.RemoveRange(await _context.ClientPostLogoutRedirectUris.Where(c => c.ClientId == client.Id).ToListAsync());
            
            _context.ClientGrantTypes.RemoveRange(await _context.ClientGrantTypes.Where(c => c.ClientId == client.Id).ToListAsync());
            
            _context.ClientCorsOrigins.RemoveRange(await _context.ClientCorsOrigins.Where(c => c.ClientId == client.Id).ToListAsync());
          
            _context.ClientScopes.RemoveRange(await _context.ClientScopes.Where(c => c.ClientId == client.Id).ToListAsync());
            
            _context.SaveChanges();
            
            
            client.ClientName = updateClientRepoParam.ClientName;
            client.Description = updateClientRepoParam.Description;
            client.RequirePkce = updateClientRepoParam.RequirePkce;
            client.AllowOfflineAccess = updateClientRepoParam.AllowOfflineAccess;
            client.AlwaysSendClientClaims = updateClientRepoParam.AlwaysSendClientClaims;
            client.LastAccessed = updateClientRepoParam.LastAccessed;
            
            
            var clientGrantTypes= new List<ClientGrantType>();        
            
            foreach (var grantType in updateClientRepoParam.AllowedGrantTypes)
            {
                
                clientGrantTypes.Add(new ClientGrantType
                {
                    GrantType = grantType,
                    ClientId = client.Id
                });
            }
            
            client.AllowedGrantTypes=clientGrantTypes;

            if (updateClientRepoParam.RedirectUris!=null)
            {
                
                var clientRedirectUris= new List<ClientRedirectUri>();
                
                foreach (var redirectUri in updateClientRepoParam.RedirectUris)
                {
                    
                    clientRedirectUris.Add(new ClientRedirectUri
                    {
                        RedirectUri = redirectUri,
                        ClientId = client.Id
                    });
                }
                client.RedirectUris = clientRedirectUris;
            }


            if (updateClientRepoParam.PostLogoutRedirectUris!=null)
            {
                
                var clientPostLogoutRedirectUriEntity = new List<ClientPostLogoutRedirectUri>();
                
                foreach (var logoutRedirectUri in updateClientRepoParam.PostLogoutRedirectUris)
                {
                    
                    clientPostLogoutRedirectUriEntity.Add(new ClientPostLogoutRedirectUri
                    {
                        PostLogoutRedirectUri = logoutRedirectUri,
                        ClientId = client.Id
                    });
                }
                
                client.PostLogoutRedirectUris = clientPostLogoutRedirectUriEntity;
            }

            if (updateClientRepoParam.AllowedScopes!=null)
            {
                
                var clientScope=new List<ClientScope>();
                
                foreach (var scope in updateClientRepoParam.AllowedScopes)
                {
                    
                    clientScope.Add(new ClientScope
                    {
                        Scope = scope,
                        ClientId = client.Id
                    });
                }
                
                client.AllowedScopes = clientScope;
            }

            if (updateClientRepoParam.Claims!=null)
            {
                
                var clientClaim=new List<ClientClaim>();
                
                foreach (var claim in updateClientRepoParam.Claims)
                {
                    clientClaim.Add(new ClientClaim
                    {
                        ClientId = client.Id,
                        Type = claim,
                        Value = claim
                    });
                }
                
                client.Claims = clientClaim;
            }

            if (updateClientRepoParam.AllowedCorsOrigins!=null)
            {
                
                var clientCorsOrigin=new List<ClientCorsOrigin>();
                
                foreach (var cors in updateClientRepoParam.AllowedCorsOrigins)
                {
                    
                    clientCorsOrigin.Add(new ClientCorsOrigin
                    {
                        ClientId = client.Id,
                        Origin = cors
                    });
                }
                
                client.AllowedCorsOrigins = clientCorsOrigin;

            }

            _context.Client.Update(client);
            await _context.SaveChangesAsync();
            
            return new CreateClientRepoResult(client);
        }
        

        public async Task<List<CreateClientRepoResult>> GetAllClientsAsync()
        {
            var allClients = await _context.Client.ToListAsync();
            
            if (allClients == null)
            {
                _logger.LogError("no client in DB,GetAllClientsAsync in ClientRepository");
                throw new Exception("no client in DB,GetAllClientsAsync in ClientRepository");
            }

            var clients = new List<CreateClientRepoResult>();
            
            foreach (var client in allClients)
            {
                clients.Add(new CreateClientRepoResult(client));
            }

            return clients;
        }

        public async Task<CreateClientRepoResult> GetOneClientAsync(int? id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, GetOneClientAsync ClientRepository");
                throw new Exception("id is null, GetOneClientAsync ClientRepository");
            }
            
            var client = await _context.Client
                .Include(c=> c.Claims)
                .Include(c=>c.AllowedScopes)
                .Include(c=>c.RedirectUris)
                .Include(c=> c.AllowedCorsOrigins)
                .Include(c=>c.AllowedGrantTypes)
                .Include(c=>c.PostLogoutRedirectUris)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (client == null)
            {
                _logger.LogError("client not found in DB,GetOneClientAsync ClientRepository");
                throw new Exception("client not found in DB,GetOneClientAsync ClientRepository");
            }
            
            return new CreateClientRepoResult(client);

        }
        
        public async Task DeleteClientAsync(int id)
        {
            
            if (id.ToString()==null)
            {
                _logger.LogError("id is null,DeleteClientAsync ClientRepository");
                throw new Exception("Id is null");
            }

            try
            {
                var client = await _context.Client.FirstOrDefaultAsync(c => c.Id == id);
                
                if (client==null)
                {
                    _logger.LogError("client is null,DeleteClientAsync ClientRepository");
                    throw new Exception("client is null,DeleteClientAsync ClientRepository");
                }
                
                _context.Client.Remove(client);
                await _context.SaveChangesAsync();
                
            }
            catch (Exception)
            {
                _logger.LogError("client not removed,DeleteClientAsync ClientRepository");
                throw new Exception("client didnt remove");
            }


        }
        
    }
}