using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Data;
using Identity.Models.Entities.ApiResourcesEntity;
using Identity.Models.RepoParams.ApiResources;
using Identity.Models.RepoResults.ApiResources;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Identity.Repository
{
    public interface IApiResourcesRepository
    {
        Task<CreateApiResourcesRepoResult> CreateApiResourcesAsync(
            CreateApiResourcesRepoParam createApiResourcesRepoParam);

        Task<CreateApiResourcesRepoResult> UpdateApiResourcesAsync(
            UpdateApiResourcesRepoParam updateApiResourcesRepoParam, int id);

        Task<List<CreateApiResourcesRepoResult>> GetAllApiResourcesAsync();
        Task<CreateApiResourcesRepoResult> GetOneApiResourcesAsync(int? id);
        Task DeleteApiResourcesAsync(int id);
    }
    public class ApiResourcesRepository : IApiResourcesRepository
    {
        
        private readonly ILogger<IApiResourcesRepository> _logger;
        
        private readonly MvcIdentityDbContext _context;
        
        public ApiResourcesRepository(ILogger<IApiResourcesRepository> logger, MvcIdentityDbContext context)
        {
            _context = context;
            _logger = logger;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        
        public async Task<CreateApiResourcesRepoResult> CreateApiResourcesAsync(
            CreateApiResourcesRepoParam createApiResourcesRepoParam)
        {
            
            if (createApiResourcesRepoParam == null)
            {
                _logger.LogError("createApiResourcesRepoParam is null ,CreateApiResourcesAsync in ApiResourcesRepository");
                
                throw new Exception("createApiResourcesRepoParam is null ,CreateApiResourcesAsync in ApiResourcesRepository");
            }

            var apiResources = new ApiResources
            {
                Name = createApiResourcesRepoParam.Name,
                Description = createApiResourcesRepoParam.Description,
                DisplayName = createApiResourcesRepoParam.DisplayName,
                Created = DateTime.Now
            };
            
            await _context.ApiResources.AddAsync(apiResources);
            await _context.SaveChangesAsync();
            
            return new CreateApiResourcesRepoResult(apiResources);
        }
        public async Task<CreateApiResourcesRepoResult> UpdateApiResourcesAsync(
            UpdateApiResourcesRepoParam updateApiResourcesRepoParam,int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, UpdateApiResourcesAsync ApiResourcesRepository");
                throw new Exception("id is null, UpdateApiResourcesAsync in ApiResourcesRepository");

            }
            
            if (updateApiResourcesRepoParam == null)
            {
                _logger.LogError("updateApiResourcesRepoParam is null ,UpdateApiResourcesAsync in ApiResourcesRepository");
                throw new Exception("updateApiResourcesRepoParam is null ,UpdateApiResourcesAsync in ApiResourcesRepository");
            }

            var apiResources = await _context.ApiResources.FirstOrDefaultAsync(c => c.Id == id);
            
            if (apiResources == null)
            {
                _logger.LogError("apiResources not found in DB,UpdateApiResourcesAsync ApiResourcesRepository ");
                throw new Exception("apiResources not found in DB,UpdateApiResourcesAsync ApiResourcesRepository");

            }
            
            var updateApiResources = new ApiResources
            {
                Id =id,
                Name= updateApiResourcesRepoParam.Name,
                DisplayName = updateApiResourcesRepoParam.DisplayName,
                Description = updateApiResourcesRepoParam.Description,
                Created = updateApiResourcesRepoParam.Created,
                Update = DateTime.Now
            };
            
            _context.ApiResources.Update(updateApiResources);
            await _context.SaveChangesAsync();
            
            return new CreateApiResourcesRepoResult(updateApiResources);
            
        }
        public async Task<List<CreateApiResourcesRepoResult>> GetAllApiResourcesAsync()
        {
            
            var allApiResources = await _context.ApiResources.ToListAsync();
            
            if (allApiResources == null)
            {
                _logger.LogError("no allApiResources in DB,GetAllApiResourcesAsync in ApiResourcesRepository");
                throw new Exception("no allApiResources in DB,GetAllApiResourcesAsync in ApiResourcesRepository");
            }
            

            var apiResourcesRepoResults = new List<CreateApiResourcesRepoResult>();
            
            foreach (var apiResources in allApiResources)
            {
                apiResourcesRepoResults.Add(new CreateApiResourcesRepoResult(apiResources));
            }

            return apiResourcesRepoResults;
        }
        
        public async Task<CreateApiResourcesRepoResult> GetOneApiResourcesAsync(int? id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, GetOneApiResourceAsync ApiResourcesRepository");
                throw new Exception("id is null, GetOneApiResourceAsync ApiResourcesRepository");
            }

            var apiResources = await _context.ApiResources.FirstOrDefaultAsync(c => c.Id == id);

            if (apiResources == null)
            {
                _logger.LogError("apiResources not found in DB,GetOneApiResourceAsync ApiResourcesRepository");
                throw new Exception("apiResources not found in DB,GetOneApiResourceAsync ApiResourcesRepository");
            }
            
            return new CreateApiResourcesRepoResult(apiResources);
        }
        
        public async Task DeleteApiResourcesAsync(int id)
        {
            
            if (id.ToString()==null)
            {
                _logger.LogError("id is null,DeleteApiResourceAsync ApiResourcesRepository");
                throw new Exception("Id is null");
            }

            try
            {
                var apiResource =
                    await _context.ApiResources.FirstOrDefaultAsync(apiResources => apiResources.Id == id);
                
                if (apiResource==null)
                {
                    _logger.LogError("apiResource is null,DeleteApiResourceAsync ApiResourcesRepository");
                    throw new Exception("apiResource is null,DeleteApiResourceAsync ApiResourcesRepository");
                }
                
                _context.ApiResources.Remove(apiResource);
                await _context.SaveChangesAsync();
                
            }
            
            catch (Exception)
            {
                _logger.LogError("apiResources  not removed,DeleteApiResourceAsync ApiResourcesRepository");
                throw new Exception("apiResources didnt remove");
            }
        }
    }
}