using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Data;
using Identity.Models.Entities.ClientCorsOriginEntity;
using Identity.Models.RepoParams.ClientCorsOrigin;
using Identity.Models.RepoResults.ClientCorsOrigin;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Identity.Repository
{
    public interface IClientCorsOriginRepository
    {
        Task DeleteClientCorsOriginAsync(int id);
        Task<CreateClientCorsOriginRepoResult> GetOneClientCorsOriginAsync(int? id);
        Task<List<CreateClientCorsOriginRepoResult>> GetAllClientCorsOriginsAsync();

        Task<CreateClientCorsOriginRepoResult> UpdateClientCorsOriginAsync(
            UpdateClientCorsOriginRepoParam updateClientCorsOriginRepoParam, int id);

        Task<CreateClientCorsOriginRepoResult> CreateClientCorsOriginAsync(
            CreateClientCorsOriginRepoParam createClientCorsOriginRepoParam);
    }
    public class ClientCorsOriginRepository : IClientCorsOriginRepository
    {
        
        private readonly ILogger<IClientCorsOriginRepository> _logger;
        
        private readonly MvcIdentityDbContext _context;
        
        public ClientCorsOriginRepository(ILogger<IClientCorsOriginRepository> logger, MvcIdentityDbContext context)
        {
            _context = context;
            _logger = logger;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        
        public async Task<CreateClientCorsOriginRepoResult> CreateClientCorsOriginAsync(
            CreateClientCorsOriginRepoParam createClientCorsOriginRepoParam)
        {
            
            if (createClientCorsOriginRepoParam == null)
            {
                _logger.LogError("CreateClientCorsOriginRepoParam is null ,CreateClientCorsOriginAsync in ClientCorsOriginRepository");
                throw new Exception("CreateClientCorsOriginRepoParam is null ,CreateClientCorsOriginAsync in ClientCorsOriginRepository");
            }

            var clientCorsOrigin = new ClientCorsOrigin
            {
                Origin = createClientCorsOriginRepoParam.Origin,
                ClientId = createClientCorsOriginRepoParam.ClientId,
            };
            
            await _context.ClientCorsOrigins.AddAsync(clientCorsOrigin);
            await _context.SaveChangesAsync();
            
            return new CreateClientCorsOriginRepoResult(clientCorsOrigin);
        }
        public async Task<CreateClientCorsOriginRepoResult> UpdateClientCorsOriginAsync(
            UpdateClientCorsOriginRepoParam updateClientCorsOriginRepoParam,int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, UpdateClientCorsOriginAsync ClientCorsOriginRepository");
                throw new Exception("id is null, UpdateClientCorsOriginAsync in ClientCorsOriginRepository");
            }
            
            if (updateClientCorsOriginRepoParam == null)
            {
                _logger.LogError("updateClientCorsOriginRepoParam is null ,UpdateClientCorsOriginAsync in ClientCorsOriginRepository");
                throw new Exception("updateClientCorsOriginRepoParam is null ,UpdateClientCorsOriginAsync in ClientCorsOriginRepository");
            }

            var clientCorsOrigin = await _context.ClientCorsOrigins.FirstOrDefaultAsync(c => c.Id == id);
            
            if (clientCorsOrigin == null)
            {
                
                _logger.LogError("clientCorsOrigin not found in DB,UpdateClientCorsOriginAsync ClientCorsOriginRepository ");
                throw new Exception("clientCorsOrigin not found in DB,UpdateClientCorsOriginAsync ClientCorsOriginRepository");

            }
            
            var updateClientCorsOrigin = new ClientCorsOrigin
            {
                Id =id,
                ClientId = updateClientCorsOriginRepoParam.ClientId,
                Origin = updateClientCorsOriginRepoParam.Origin
            };
            
            _context.ClientCorsOrigins.Update(updateClientCorsOrigin);
            await _context.SaveChangesAsync();
            
            return new CreateClientCorsOriginRepoResult(updateClientCorsOrigin);
            
        }
        public async Task<List<CreateClientCorsOriginRepoResult>> GetAllClientCorsOriginsAsync()
        {
            var allClientCorsOrigins = await _context.ClientCorsOrigins.ToListAsync();
            
            if (allClientCorsOrigins == null)
            {
                _logger.LogError("no ClientCorsOrigin in DB,GetAllClientCorsOriginsAsync in ClientCorsOriginRepository");
                throw new Exception("no ClientCorsOrigin in DB,GetAllClientCorsOriginsAsync in ClientCorsOriginRepository");

            }

            var clientCorsOrigins = new List<CreateClientCorsOriginRepoResult>();
            
            foreach (var clientCorsOrigin in allClientCorsOrigins)
            {
                clientCorsOrigins.Add(new CreateClientCorsOriginRepoResult(clientCorsOrigin));
            }

            return clientCorsOrigins;
        }
        
        public async Task<CreateClientCorsOriginRepoResult> GetOneClientCorsOriginAsync(int? id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, GetOneClientCorsOriginAsync ClientCorsOriginRepository");
                throw new Exception("id is null, GetOneClientCorsOriginAsync ClientCorsOriginRepository");
            }

            var clientCorsOrigin = await _context.ClientCorsOrigins.FirstOrDefaultAsync(c => c.Id == id);

            if (clientCorsOrigin == null)
            {
                _logger.LogError("clientCorsOrigin not found in DB,GetOneClientCorsOriginAsync ClientCorsOriginRepository");
                throw new Exception("clientCorsOrigin not found in DB,GetOneClientCorsOriginAsync ClientCorsOriginRepository");
            }
            
            return new CreateClientCorsOriginRepoResult(clientCorsOrigin);

        }
        
        public async Task DeleteClientCorsOriginAsync(int id)
        {
            if (id.ToString()==null)
            {
                _logger.LogError("id is null,DeleteClientCorsOriginAsync ClientCorsOriginRepository");
                throw new Exception("Id is null");
            }

            try
            {
                var corsOrigin =
                    await _context.ClientCorsOrigins.FirstOrDefaultAsync(c => c.Id == id);
                
                if (corsOrigin==null)
                {
                    _logger.LogError("corsOrigin is null,DeleteClientCorsOriginAsync ClientCorsOriginRepository");
                    throw new Exception("corsOrigin is null,DeleteClientCorsOriginAsync ClientCorsOriginRepository");
                }
                
                _context.ClientCorsOrigins.Remove(corsOrigin);
                await _context.SaveChangesAsync();
                
            }
            catch (Exception)
            {
                _logger.LogError("ClientCorsOrigins  not removed,DeleteClientCorsOriginAsync ClientCorsOriginRepository");
                throw new Exception("ClientCorsOrigins didnt remove");
            }
        }
        
    }
}