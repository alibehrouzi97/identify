using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Data;
using Identity.Models.Entities.ClientClaimEntity;
using Identity.Models.RepoParams.ClientClaim;
using Identity.Models.RepoResults.Client;
using Identity.Models.RepoResults.ClientClaim;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Identity.Repository
{
    public interface IClientClaimRepository
    {
        Task<CreateClientClaimRepoResult> GetOneClientClaimAsync(int? id);

        Task<CreateClientClaimRepoResult> CreateClientClaimAsync(
            CreateClientClaimRepoParam createClientClaimRepoParam);

        Task<CreateClientClaimRepoResult> UpdateClientClaimAsync(
            UpdateClientClaimRepoParam updateClientClaimRepoParam, int id);

        Task<List<CreateClientClaimRepoResult>> GetAllClientClaimsAsync();
        Task DeleteClientClaimAsync(int id);

    }
    public class ClientClaimRepository : IClientClaimRepository
    {
        private readonly ILogger<IClientClaimRepository> _logger;
        
        private readonly MvcIdentityDbContext _context;
        
        public ClientClaimRepository(ILogger<IClientClaimRepository> logger, MvcIdentityDbContext context)
        {
            _context = context;
            _logger = logger;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            
        }

        public async Task<CreateClientClaimRepoResult> CreateClientClaimAsync(
            CreateClientClaimRepoParam createClientClaimRepoParam)
        {
            
            if (createClientClaimRepoParam == null)
            {
                _logger.LogError("CreateClientClaimRepoParam is null ,CreateClientClaimAsync in ClientClaimRepository");
                throw new Exception("CreateClientClaimRepoParam is null ,CreateClientClaimAsync in ClientClaimRepository");
            }

            var clientClaim = new ClientClaim
            {
                ClientId = createClientClaimRepoParam.ClientId,
                Type = createClientClaimRepoParam.Type,
                Value = createClientClaimRepoParam.Value
            };
            
            await _context.ClientClaims.AddAsync(clientClaim);
            await _context.SaveChangesAsync();
            
            return new CreateClientClaimRepoResult(clientClaim);

        }

        public async Task<CreateClientClaimRepoResult> UpdateClientClaimAsync(
            UpdateClientClaimRepoParam updateClientClaimRepoParam,int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, UpdateClientClaimAsync ClientClaimRepository");
                throw new Exception("id is null, UpdateClientClaimAsync ClientClaimRepository");

            }
            
            if (updateClientClaimRepoParam == null)
            {
                _logger.LogError("updateClientClaimRepoParam is null ,UpdateClientClaimAsync in ClientClaimRepository");
                throw new Exception("updateClientClaimRepoParam is null ,UpdateClientClaimAsync in ClientClaimRepository");
            }

            var clientClaim = await _context.ClientClaims.FirstOrDefaultAsync(c => c.Id == id);
            
            if (clientClaim == null)
            {
                
                _logger.LogError("clientClaim not found in DB,UpdateClientClaimAsync ClientClaimRepository ");
                throw new Exception("clientClaim not found in DB,UpdateClientClaimAsync ClientClaimRepository");
            }
            
            var updateClientClaim = new ClientClaim
            {
                Id =id,
                ClientId = updateClientClaimRepoParam.ClientId,
                Type = updateClientClaimRepoParam.Type,
                Value = updateClientClaimRepoParam.Value
            };
            
            _context.ClientClaims.Update(updateClientClaim);
            await _context.SaveChangesAsync();
            
            return new CreateClientClaimRepoResult(updateClientClaim);
            
        }
        public async Task<List<CreateClientClaimRepoResult>> GetAllClientClaimsAsync()
        {
            var allClientClaims = await _context.ClientClaims.ToListAsync();
            
            if (allClientClaims == null)
            {
                _logger.LogError("no clientClaim in DB,GetAllClientClaimsAsync in ClientClaimRepository");
                throw new Exception("no clientClaim in DB,GetAllClientClaimsAsync in ClientClaimRepository");
            }

            var clientClaims = new List<CreateClientClaimRepoResult>();
            
            foreach (var clientClaim in allClientClaims)
            {
                clientClaims.Add(new CreateClientClaimRepoResult(clientClaim));
            }

            return clientClaims;
        }

        public async Task<CreateClientClaimRepoResult> GetOneClientClaimAsync(int? id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, GetOneClientClaimAsync ClientClaimRepository");
                throw new Exception("id is null, GetOneClientClaimAsync ClientClaimRepository");
            }

            var clientClaim = await _context.ClientClaims.FirstOrDefaultAsync(c => c.Id == id);

            if (clientClaim == null)
            {
                _logger.LogError("clientClaim not found in DB,GetOneClientClaimAsync ClientClaimRepository");
                throw new Exception("clientClaim not found in DB,GetOneClientClaimAsync ClientClaimRepository");
            }
            
            return new CreateClientClaimRepoResult(clientClaim);
            
        }
        
        public async Task DeleteClientClaimAsync(int id)
        {
            
            if (id.ToString()==null)
            {
                _logger.LogError("id is null,DeleteClientClaimAsync ClientClaimRepository");
                throw new Exception("Id is null");
            }

            try
            {
                var clientClaim = await _context.ClientClaims.FirstOrDefaultAsync(c => c.Id == id);
                
                if (clientClaim==null)
                {
                    _logger.LogError("clientClaim is null,DeleteClientClaimAsync ClientClaimRepository");
                    throw new Exception("clientClaim is null,DeleteClientClaimAsync ClientClaimRepository");
                }
                
                _context.ClientClaims.Remove(clientClaim);
                await _context.SaveChangesAsync();
            }
            
            catch (Exception)
            {
                _logger.LogError("clientClaim  not removed,DeleteClientClaimAsync ClientClaimRepository");
                throw new Exception("clientClaim didnt remove");
            }
            
        }
    }
}