using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Data;
using Identity.Models.Entities.ClientGrantTypeEntity;
using Identity.Models.RepoParams.ClientGrantType;
using Identity.Models.RepoResults.ClientGrantType;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Identity.Repository
{
    public interface IClientGrantTypeRepository
    {
        Task DeleteClientGrantTypeAsync(int id);
        Task<List<CreateClientGrantTypeRepoResult>> GetAllClientGrantTypesAsync();

        Task<CreateClientGrantTypeRepoResult> UpdateClientGrantTypeAsync(
            UpdateClientGrantTypeRepoParam updateClientGrantTypeRepoParam, int id);

        Task<CreateClientGrantTypeRepoResult> GetOneClientGrantTypeAsync(int? id);

        Task<CreateClientGrantTypeRepoResult> CreateClientGrantTypeAsync(
            CreateClientGrantTypeRepoParam createClientGrantTypeRepoParam);
    }
    public class ClientGrantTypeRepository : IClientGrantTypeRepository
    {
        
        private readonly ILogger<IClientGrantTypeRepository> _logger;
        
        private readonly MvcIdentityDbContext _context;
        
        public ClientGrantTypeRepository(ILogger<IClientGrantTypeRepository> logger, MvcIdentityDbContext context)
        {
            _context = context;
            _logger = logger;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        
        public async Task<CreateClientGrantTypeRepoResult> CreateClientGrantTypeAsync(
            CreateClientGrantTypeRepoParam createClientGrantTypeRepoParam)
        {
            
            if (createClientGrantTypeRepoParam == null)
            {
                _logger.LogError("CreateClientGrantTypeRepoParam is null ,CreateClientGrantTypeAsync in ClientGrantTypeRepository");
                throw new Exception("CreateClientGrantTypeRepoParam is null ,CreateClientGrantTypeAsync in ClientGrantTypeRepository");
            }

            var clientGrantType = new ClientGrantType
            {
                GrantType = createClientGrantTypeRepoParam.GrantType,
                ClientId = createClientGrantTypeRepoParam.ClientId,
            };
            
            await _context.ClientGrantTypes.AddAsync(clientGrantType);
            await _context.SaveChangesAsync();
            
            return new CreateClientGrantTypeRepoResult(clientGrantType);
        }
        public async Task<CreateClientGrantTypeRepoResult> UpdateClientGrantTypeAsync(
            UpdateClientGrantTypeRepoParam updateClientGrantTypeRepoParam,int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, UpdateClientGrantTypeAsync in ClientGrantTypeRepository");
                throw new Exception("id is null, UpdateClientGrantTypeAsync in ClientGrantTypeRepository");

            }
            
            if (updateClientGrantTypeRepoParam == null)
            {
                _logger.LogError("updateClientGrantTypeRepoParam is null ,UpdateClientGrantTypeAsync in ClientGrantTypeRepository");
                throw new Exception("updateClientGrantTypeRepoParam is null ,UpdateClientGrantTypeAsync in ClientGrantTypeRepository");
            }

            var clientGrantType = await _context.ClientGrantTypes.FirstOrDefaultAsync(c => c.Id == id);
            
            if (clientGrantType == null)
            {
                
                _logger.LogError("clientGrantType not found in DB,UpdateClientGrantTypeAsync ClientGrantTypeRepository ");
                throw new Exception("clientGrantType not found in DB,UpdateClientGrantTypeAsync ClientGrantTypeRepository");

            }
            
            var updateClientGrantType = new ClientGrantType
            {
                Id =id,
                ClientId = updateClientGrantTypeRepoParam.ClientId,
                GrantType = updateClientGrantTypeRepoParam.GrantType
            };
            
            _context.ClientGrantTypes.Update(updateClientGrantType);
            await _context.SaveChangesAsync();
            
            return new CreateClientGrantTypeRepoResult(updateClientGrantType);
            
        }
        public async Task<List<CreateClientGrantTypeRepoResult>> GetAllClientGrantTypesAsync()
        {
            
            var allClientGrantTypes = await _context.ClientGrantTypes.ToListAsync();
            
            if (allClientGrantTypes == null)
            {
                _logger.LogError("no ClientGrantType in DB,GetAllClientGrantTypesAsync in ClientGrantTypeRepository");
                throw new Exception("no ClientGrantType in DB,GetAllClientGrantTypesAsync in ClientGrantTypeRepository");
            }

            var clientGrantTypeRepoResults = new List<CreateClientGrantTypeRepoResult>();
            
            foreach (var clientGrantType in allClientGrantTypes)
            {
                clientGrantTypeRepoResults.Add(new CreateClientGrantTypeRepoResult(clientGrantType));
            }

            return clientGrantTypeRepoResults;
        }
        public async Task<CreateClientGrantTypeRepoResult> GetOneClientGrantTypeAsync(int? id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, GetOneClientGrantTypeAsync ClientGrantTypeRepository");
                throw new Exception("id is null, GetOneClientGrantTypeAsync ClientGrantTypeRepository");
            }
            
            var clientGrantType = await _context.ClientGrantTypes.FirstOrDefaultAsync(c => c.Id == id);

            if (clientGrantType == null)
            {
                _logger.LogError("clientGrantType not found in DB,GetOneClientGrantTypeAsync ClientGrantTypeRepository");
                throw new Exception("clientGrantType not found in DB,GetOneClientGrantTypeAsync ClientGrantTypeRepository");
            }
            
            return new CreateClientGrantTypeRepoResult(clientGrantType);
           
        }
        public async Task DeleteClientGrantTypeAsync(int id)
        {
            if (id.ToString()==null)
            {
                _logger.LogError("id is null,DeleteClientGrantTypeAsync ClientGrantTypeRepository");
                throw new Exception("Id is null");
            }

            try
            {

                var grantType =
                    await _context.ClientGrantTypes.FirstOrDefaultAsync(clientGrantType => clientGrantType.Id == id);
                
                if (grantType==null)
                {
                    _logger.LogError("grantType is null,DeleteClientGrantTypeAsync ClientGrantTypeRepository");
                    throw new Exception("grantType is null,DeleteClientGrantTypeAsync ClientGrantTypeRepository");
                }
                
                _context.ClientGrantTypes.Remove(grantType);
                await _context.SaveChangesAsync();
                
            }
            catch (Exception)
            {
                _logger.LogError("ClientGrantType  not removed,DeleteClientGrantTypeAsync ClientGrantTypeRepository");
                throw new Exception("ClientGrantType didnt remove");
            }
        }
        
        
        
    }
}