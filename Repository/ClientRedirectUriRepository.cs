using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Data;
using Identity.Models.Entities.ClientPostLogoutRedirectUriEntity;
using Identity.Models.Entities.ClientRedirectUriEntity;
using Identity.Models.RepoParams.ClientPostLogoutRedirectUri;
using Identity.Models.RepoParams.ClientRedirectUriEntity;
using Identity.Models.RepoResults.ClientRedirectUri;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Identity.Repository
{
    public interface IClientRedirectUriRepository
    {
        Task<CreateClientRedirectUriRepoResult> CreateClientRedirectUriAsync(
            CreateClientRedirectUriRepoParam createClientRedirectUriRepoParam);

        Task<CreateClientRedirectUriRepoResult> UpdateClientRedirectUriAsync(
            UpdateClientRedirectUriRepoParam updateClientRedirectUriRepoParam, int id);

        Task<List<CreateClientRedirectUriRepoResult>> GetAllClientRedirectUrisAsync();
        Task<CreateClientRedirectUriRepoResult> GetOneClientRedirectUriAsync(int? id);
        Task DeleteClientRedirectUriAsync(int id);
    }
    public class ClientRedirectUriRepository : IClientRedirectUriRepository
    {
        
        private readonly ILogger<IClientRedirectUriRepository> _logger;
        
        private readonly MvcIdentityDbContext _context;
        
        public ClientRedirectUriRepository(ILogger<IClientRedirectUriRepository> logger, MvcIdentityDbContext context)
        {
            _context = context;
            _logger = logger;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        
        public async Task<CreateClientRedirectUriRepoResult> CreateClientRedirectUriAsync(
            CreateClientRedirectUriRepoParam createClientRedirectUriRepoParam)
        {
            
            if (createClientRedirectUriRepoParam == null)
            {
                _logger.LogError("createClientRedirectUriRepoParam is null ,CreateClientRedirectUriAsync in ClientRedirectUriRepository");
                throw new Exception("createClientRedirectUriRepoParam is null ,CreateClientRedirectUriAsync in ClientRedirectUriRepository");
            }

            var clientRedirectUri = new ClientRedirectUri
            {
                RedirectUri = createClientRedirectUriRepoParam.RedirectUri,
                ClientId = createClientRedirectUriRepoParam.ClientId,
            };
            
            await _context.ClientRedirectUris.AddAsync(clientRedirectUri);
            await _context.SaveChangesAsync();
            
            return new CreateClientRedirectUriRepoResult(clientRedirectUri);
        }
        
        public async Task<CreateClientRedirectUriRepoResult> UpdateClientRedirectUriAsync(
            UpdateClientRedirectUriRepoParam updateClientRedirectUriRepoParam,int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, UpdateClientRedirectUriAsync ClientRedirectUriRepository");
                throw new Exception("id is null, UpdateClientRedirectUriAsync ClientRedirectUriRepository");

            }
            
            if (updateClientRedirectUriRepoParam == null)
            {
                _logger.LogError("updateClientRedirectUriRepoParam is null ,UpdateClientRedirectUriAsync in ClientRedirectUriRepository");
                throw new Exception("updateClientRedirectUriRepoParam is null ,UpdateClientRedirectUriAsync in ClientRedirectUriRepository");
            }

            var clientRedirectUri = await _context.ClientRedirectUris.FirstOrDefaultAsync(c => c.Id == id);
            
            if (clientRedirectUri == null)
            {
                
                _logger.LogError("clientRedirectUri not found in DB,UpdateClientRedirectUriAsync ClientRedirectUriRepository ");
                throw new Exception("clientRedirectUri not found in DB,UpdateClientRedirectUriAsync ClientRedirectUriRepository");

            }
            
            var redirectUri = new ClientRedirectUri
            {
                Id =id,
                ClientId = updateClientRedirectUriRepoParam.ClientId,
                RedirectUri = updateClientRedirectUriRepoParam.RedirectUri
            };
            
            _context.ClientRedirectUris.Update(redirectUri);
            await _context.SaveChangesAsync();
            
            return new CreateClientRedirectUriRepoResult(redirectUri);
            
        }
        public async Task<List<CreateClientRedirectUriRepoResult>> GetAllClientRedirectUrisAsync()
        {
            
            var clientRedirectUris = await _context.ClientRedirectUris.ToListAsync();
            
            if (clientRedirectUris == null)
            {
                _logger.LogError("no clientRedirectUri in DB,GetAllClientRedirectUrisAsync in ClientRedirectUriRepository");
                throw new Exception("no clientRedirectUri in DB,GetAllClientRedirectUrisAsync in ClientRedirectUriRepository");

            }

            var clientRedirectUriRepoResults = new List<CreateClientRedirectUriRepoResult>();
            
            foreach (var clientRedirectUri in clientRedirectUris)
            {
                clientRedirectUriRepoResults.Add(new CreateClientRedirectUriRepoResult(clientRedirectUri));
            }

            return clientRedirectUriRepoResults;
        }
        public async Task<CreateClientRedirectUriRepoResult> GetOneClientRedirectUriAsync(int? id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, GetOneClientRedirectUriAsync ClientRedirectUriRepository");
                throw new Exception("id is null, GetOneClientRedirectUriAsync ClientRedirectUriRepository");
            }
            
            var clientRedirectUri = await _context.ClientRedirectUris.FirstOrDefaultAsync(c => c.Id == id);

            if (clientRedirectUri == null)
            {
                _logger.LogError("clientRedirectUri not found in DB,GetOneClientRedirectUriAsync ClientRedirectUriRepository");
                throw new Exception("clientRedirectUri not found in DB,GetOneClientRedirectUriAsync ClientRedirectUriRepository");
            }
            
            return new CreateClientRedirectUriRepoResult(clientRedirectUri);

        }
        public async Task DeleteClientRedirectUriAsync(int id)
        {
            if (id.ToString()==null)
            {
                _logger.LogError("id is null,DeleteClientRedirectUriAsync ClientRedirectUriRepository");
                throw new Exception("Id is null");
            }

            try
            {
                
                var redirectUri= await _context.ClientRedirectUris.FirstOrDefaultAsync(clientRedirectUri =>
                    clientRedirectUri.Id == id);
                
                if (redirectUri==null)
                {
                    _logger.LogError("redirectUri is null,DeleteClientRedirectUriAsync ClientRedirectUriRepository");
                    throw new Exception("redirectUri is null,DeleteClientRedirectUriAsync ClientRedirectUriRepository");
                }
                
                _context.ClientRedirectUris.Remove(redirectUri);
                await _context.SaveChangesAsync();
                
            }
            catch (Exception)
            {
                _logger.LogError("clientRedirectUri  not removed,DeleteClientRedirectUriAsync ClientRedirectUriRepository");
                throw new Exception("clientRedirectUri didnt remove");
            }
        }
    }
}