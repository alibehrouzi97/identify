using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Data;
using Identity.Models.Entities.ClientGrantTypeEntity;
using Identity.Models.Entities.ClientPostLogoutRedirectUriEntity;
using Identity.Models.RepoParams.ClientPostLogoutRedirectUri;
using Identity.Models.RepoResults.ClientGrantType;
using Identity.Models.RepoResults.ClientPostLogoutRedirectUri;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Identity.Repository
{
    public interface IClientPostLogoutRedirectUriRepository
    {
        Task DeleteClientPostLogoutRedirectUriAsync(int id);
        Task<CreateClientPostLogoutRedirectUriRepoResult> GetOneClientPostLogoutRedirectUriAsync(int? id);
        
        Task<List<CreateClientPostLogoutRedirectUriRepoResult>> GetAllClientPostLogoutRedirectUrisAsync();

        Task<CreateClientPostLogoutRedirectUriRepoResult> UpdateClientPostLogoutRedirectUriAsync(
            UpdateClientPostLogoutRedirectUriRepoParam updateClientPostLogoutRedirectUriRepoParam, int id);

        Task<CreateClientPostLogoutRedirectUriRepoResult> CreateClientPostLogoutRedirectUriAsync(
            CreateClientPostLogoutRedirectUriRepoParam createClientPostLogoutRedirectUriRepoParam);
    }
    public class ClientPostLogoutRedirectUriRepository : IClientPostLogoutRedirectUriRepository
    {
        
        private readonly ILogger<IClientPostLogoutRedirectUriRepository> _logger;
        
        private readonly MvcIdentityDbContext _context;
        
        public ClientPostLogoutRedirectUriRepository(ILogger<IClientPostLogoutRedirectUriRepository> logger, MvcIdentityDbContext context)
        {
            _context = context;
            _logger = logger;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        
        public async Task<CreateClientPostLogoutRedirectUriRepoResult> CreateClientPostLogoutRedirectUriAsync(
            CreateClientPostLogoutRedirectUriRepoParam createClientPostLogoutRedirectUriRepoParam)
        {
            
            if (createClientPostLogoutRedirectUriRepoParam == null)
            {
                _logger.LogError("CreateClientPostLogoutRedirectUriRepoParam is null ,CreateClientRedirectUriAsync in ClientPostLogoutRedirectUriRepository");
                throw new Exception("CreateClientPostLogoutRedirectUriRepoParam is null ,CreateClientRedirectUriAsync in ClientPostLogoutRedirectUriRepository");
            }

            var clientPostLogoutRedirectUri = new ClientPostLogoutRedirectUri
            {
                PostLogoutRedirectUri = createClientPostLogoutRedirectUriRepoParam.PostLogoutRedirectUri,
                ClientId = createClientPostLogoutRedirectUriRepoParam.ClientId,
            };
            
            await _context.ClientPostLogoutRedirectUris.AddAsync(clientPostLogoutRedirectUri);
            await _context.SaveChangesAsync();
            
            return new CreateClientPostLogoutRedirectUriRepoResult(clientPostLogoutRedirectUri);
        }
        
        public async Task<CreateClientPostLogoutRedirectUriRepoResult> UpdateClientPostLogoutRedirectUriAsync(
            UpdateClientPostLogoutRedirectUriRepoParam updateClientPostLogoutRedirectUriRepoParam,int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, UpdateClientPostLogoutRedirectUriAsync ClientPostLogoutRedirectUriRepository");
                throw new Exception("id is null, UpdateClientPostLogoutRedirectUriAsync ClientPostLogoutRedirectUriRepository");

            }
            
            if (updateClientPostLogoutRedirectUriRepoParam == null)
            {
                _logger.LogError("UpdateClientPostLogoutRedirectUriRepoParam is null ,UpdateClientPostLogoutRedirectUriAsync in ClientPostLogoutRedirectUriRepository");
                throw new Exception("UpdateClientPostLogoutRedirectUriRepoParam is null ,UpdateClientPostLogoutRedirectUriAsync in ClientPostLogoutRedirectUriRepository");
            }

            var clientPostLogoutRedirectUri = await _context.ClientPostLogoutRedirectUris.FirstOrDefaultAsync(c => c.Id == id);
            
            if (clientPostLogoutRedirectUri == null)
            {
                
                _logger.LogError("clientPostLogoutRedirectUri not found in DB,UpdateClientPostLogoutRedirectUriAsync ClientPostLogoutRedirectUriRepository ");
                throw new Exception("clientPostLogoutRedirectUri not found in DB,UpdateClientPostLogoutRedirectUriAsync ClientPostLogoutRedirectUriRepository");

            }
            
            var postLogoutRedirectUri = new ClientPostLogoutRedirectUri
            {
                Id =id,
                ClientId = updateClientPostLogoutRedirectUriRepoParam.ClientId,
                PostLogoutRedirectUri = updateClientPostLogoutRedirectUriRepoParam.PostLogoutRedirectUri
            };
            
            _context.ClientPostLogoutRedirectUris.Update(postLogoutRedirectUri);
            await _context.SaveChangesAsync();
            
            return new CreateClientPostLogoutRedirectUriRepoResult(postLogoutRedirectUri);
            
        }
        public async Task<List<CreateClientPostLogoutRedirectUriRepoResult>> GetAllClientPostLogoutRedirectUrisAsync()
        {
            
            var postLogoutRedirectUris = await _context.ClientPostLogoutRedirectUris.ToListAsync();
            
            if (postLogoutRedirectUris == null)
            {
                _logger.LogError("no postLogoutRedirectUris in DB,GetAllClientPostLogoutRedirectUrisAsync in ClientPostLogoutRedirectUriRepository");
                throw new Exception("no postLogoutRedirectUris in DB,GetAllClientPostLogoutRedirectUrisAsync in ClientPostLogoutRedirectUriRepository");

            }

            var postLogoutRedirectUriRepoResults = new List<CreateClientPostLogoutRedirectUriRepoResult>();
            
            foreach (var postLogoutRedirectUri in postLogoutRedirectUris)
            {
                postLogoutRedirectUriRepoResults.Add(new CreateClientPostLogoutRedirectUriRepoResult(postLogoutRedirectUri));
            }

            return postLogoutRedirectUriRepoResults;
        }
        public async Task<CreateClientPostLogoutRedirectUriRepoResult> GetOneClientPostLogoutRedirectUriAsync(int? id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, GetOneClientPostLogoutRedirectUriAsync ClientPostLogoutRedirectUriRepository");
                throw new Exception("id is null, GetOneClientPostLogoutRedirectUriAsync ClientPostLogoutRedirectUriRepository");
            }
            
            var clientPostLogoutRedirectUri = await _context.ClientPostLogoutRedirectUris.FirstOrDefaultAsync(c => c.Id == id);

            if (clientPostLogoutRedirectUri == null)
            {
                _logger.LogError("clientPostLogoutRedirectUri not found in DB,GetOneClientPostLogoutRedirectUriAsync ClientPostLogoutRedirectUriRepository");
                throw new Exception("clientPostLogoutRedirectUri not found in DB,GetOneClientPostLogoutRedirectUriAsync ClientPostLogoutRedirectUriRepository");
            }
            
            return new CreateClientPostLogoutRedirectUriRepoResult(clientPostLogoutRedirectUri);

        }
        public async Task DeleteClientPostLogoutRedirectUriAsync(int id)
        {
            
            if (id.ToString()==null)
            {
                _logger.LogError("id is null,DeleteClientPostLogoutRedirectUriAsync ClientPostLogoutRedirectUriRepository");
                throw new Exception("Id is null");
            }

            try
            {

                var postLogoutRedirectUri =
                    await _context.ClientPostLogoutRedirectUris.FirstOrDefaultAsync(p =>
                        p.Id == id);
                
                if (postLogoutRedirectUri==null)
                {
                    _logger.LogError("postLogoutRedirectUri is null,DeleteClientPostLogoutRedirectUriAsync ClientPostLogoutRedirectUriRepository");
                    throw new Exception("postLogoutRedirectUri is null,DeleteClientPostLogoutRedirectUriAsync ClientPostLogoutRedirectUriRepository");
                }
                
                _context.ClientPostLogoutRedirectUris.Remove(postLogoutRedirectUri);
                await _context.SaveChangesAsync();
                
            }
            
            catch (Exception)
            {
                _logger.LogError("clientPostLogoutRedirectUri  not removed,DeleteClientPostLogoutRedirectUriAsync ClientPostLogoutRedirectUriRepository");
                throw new Exception("clientPostLogoutRedirectUri didnt remove");
            }
        }
        
        
        
    }
}