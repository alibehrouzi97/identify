namespace Identity.Models.ApiModels.ClientGrantTypeApiModels
{
    public class UpdateClientGrantTypeApiModel
    {
        public string GrantType{ get; set; }
        public int ClientId{ get; set; }
    }
}