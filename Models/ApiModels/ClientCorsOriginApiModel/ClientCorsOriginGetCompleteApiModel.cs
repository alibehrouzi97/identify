using Identity.Models.RepoResults.ClientCorsOrigin;

namespace Identity.Models.ApiModels.ClientCorsOriginApiModel
{
    public class ClientCorsOriginGetCompleteApiModel
    {
        public ClientCorsOriginGetCompleteApiModel(CreateClientCorsOriginRepoResult createClientCorsOriginRepoResult)
        {
            Id = createClientCorsOriginRepoResult.Id;
            Origin = createClientCorsOriginRepoResult.Origin;
            ClientId = createClientCorsOriginRepoResult.ClientId;
        }
        public int Id { get; set; }
        public string Origin { get; set; }
        public int ClientId { get; set; }
    }
}