namespace Identity.Models.ApiModels.ClientCorsOriginApiModel
{
    public class UpdateClientCorsOriginApiModel
    {
        public string Origin { get; set; }
        public int ClientId { get; set; }
    }
}