namespace Identity.Models.ApiModels.ClientRedirectUriApiModels
{
    public class UpdateClientRedirectUriApiModel
    {
        public string RedirectUri{ get; set; }
        public int ClientId{ get; set; }
    }
}