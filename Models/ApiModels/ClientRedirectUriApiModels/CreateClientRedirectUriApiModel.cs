namespace Identity.Models.ApiModels.ClientRedirectUriApiModels
{
    public class CreateClientRedirectUriApiModel
    {
        public string RedirectUri{ get; set; }
        public int ClientId{ get; set; }
    }
}