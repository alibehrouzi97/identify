namespace Identity.Models.ApiModels.ClientScopeApiModels
{
    public class UpdateClientScopeApiModel
    {
        public string Scope { get; set; }
        public int ClientId { get; set; }
    }
}