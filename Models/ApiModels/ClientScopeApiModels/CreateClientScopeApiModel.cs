namespace Identity.Models.ApiModels.ClientScopeApiModels
{
    public class CreateClientScopeApiModel
    {
        public string Scope { get; set; }
        public int ClientId { get; set; }
    }
}