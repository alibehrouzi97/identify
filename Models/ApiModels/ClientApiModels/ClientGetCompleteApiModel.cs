using System;
using System.Collections.Generic;
using System.Security.Claims;
using Identity.Models.Entities.ClientClaimEntity;
using Identity.Models.Entities.ClientCorsOriginEntity;
using Identity.Models.Entities.ClientEntity;
using Identity.Models.Entities.ClientGrantTypeEntity;
using Identity.Models.Entities.ClientPostLogoutRedirectUriEntity;
using Identity.Models.Entities.ClientRedirectUriEntity;
using Identity.Models.Entities.ClientScopeEntity;
using Identity.Models.RepoResults.Client;

namespace Identity.Models.ApiModels.ClientApiModels
{
    public class ClientGetCompleteApiModel
    {
        public ClientGetCompleteApiModel(CreateClientRepoResult createClientRepoResult)
        {
            Id = createClientRepoResult.Id;
            ClientName = createClientRepoResult.ClientName;
            Description = createClientRepoResult.Description;
            RequirePkce = createClientRepoResult.RequirePkce;
            AllowOfflineAccess = createClientRepoResult.AllowOfflineAccess;
            AlwaysSendClientClaims = createClientRepoResult.AlwaysSendClientClaims;
            LastAccessed = createClientRepoResult.LastAccessed;
            AllowedScopes = createClientRepoResult.AllowedScopes;
            Claims = createClientRepoResult.Claims;
            AllowedGrantTypes = createClientRepoResult.AllowedGrantTypes;
            RedirectUris = createClientRepoResult.RedirectUris;
            PostLogoutRedirectUris = createClientRepoResult.PostLogoutRedirectUris;
            AllowedCorsOrigins = createClientRepoResult.AllowedCorsOrigins;

        }
        
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string Description { get; set; }
        public bool RequirePkce { get; set; }
        
        public bool AllowOfflineAccess { get; set; }
        
        public bool AlwaysSendClientClaims { get ; set; }
        public DateTime? LastAccessed { get ; set; }
        
        public List<ClientGrantType>  AllowedGrantTypes { get; set; }
        public List<ClientRedirectUri> RedirectUris { get ; set; }
        public List<ClientPostLogoutRedirectUri> PostLogoutRedirectUris { get ; set; }
        
        public List<ClientCorsOrigin> AllowedCorsOrigins { get ; set; }
        public List<ClientScope> AllowedScopes { get ; set; }
        public List< ClientClaim> Claims { get ; set; }
    }
}