using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Identity.Models.ApiModels.ClientApiModels
{
    public class CreateClientApiModel
    {
        [Required]
        [DisplayName("Client Name : [Required**]")]
        public string ClientName { get ; set; }
        [Required]
        [DisplayName("Description : [Required**]")]
        public string  Description { get; set; }
        public DateTime? LastAccessed { get ; set; }
        public bool RequirePkce { get; set; }
        public bool AllowOfflineAccess { get; set; }
        public bool AlwaysSendClientClaims { get ; set; }

        [Required]
        [DisplayName("Allowed Grant Types : [Required**]")]
        public List<string>  AllowedGrantTypes { get; set; }
        public string RedirectUris { get ; set; }
        public string PostLogoutRedirectUris { get ; set; }
        

        public string AllowedScopes { get ; set; }
        public string Claims { get ; set; }
       
        public string AllowedCorsOrigins { get ; set; }
        

    }
}