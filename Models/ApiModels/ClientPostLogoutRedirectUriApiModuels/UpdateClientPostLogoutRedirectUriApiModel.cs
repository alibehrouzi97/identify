namespace Identity.Models.ApiModels.ClientPostLogoutRedirectUriApiModuels
{
    public class UpdateClientPostLogoutRedirectUriApiModel
    {
        public string PostLogoutRedirectUri { get; set; }
        public int ClientId { get; set; }
    }
}