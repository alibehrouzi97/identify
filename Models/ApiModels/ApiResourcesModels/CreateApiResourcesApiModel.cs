using System;

namespace Identity.Models.ApiModels.ApiResourcesModels
{
    public class CreateApiResourcesApiModel
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
    }
}