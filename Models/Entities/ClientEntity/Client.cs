using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Identity.Models.Entities.ClientClaimEntity;
using Identity.Models.Entities.ClientCorsOriginEntity;
using Identity.Models.Entities.ClientGrantTypeEntity;
using Identity.Models.Entities.ClientPostLogoutRedirectUriEntity;
using Identity.Models.Entities.ClientRedirectUriEntity;
using Identity.Models.Entities.ClientScopeEntity;

namespace Identity.Models.Entities.ClientEntity
{
    public class Client
    {
        public int Id { get; set; }
        [Required]
        public string ClientName { get; set; }
        [Required]
        public string Description { get; set; }
        
        public List<ClientGrantType> AllowedGrantTypes { get; set; }

        public bool RequirePkce { get; set; }

        public List<ClientRedirectUri> RedirectUris { get; set; }
        public List<ClientPostLogoutRedirectUri> PostLogoutRedirectUris { get; set; }
        public bool AllowOfflineAccess { get; set; }

        public List<ClientScope> AllowedScopes { get; set; }
//        
        public List<ClientClaim> Claims { get; set; }
        public bool AlwaysSendClientClaims { get; set; }
        public List<ClientCorsOrigin> AllowedCorsOrigins { get; set; }
        public DateTime? LastAccessed { get; set; }

    }
}