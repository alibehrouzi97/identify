using System;
using Identity.Models.ApiModels.ClientPostLogoutRedirectUriApiModuels;
using Identity.Models.RepoResults.ClientPostLogoutRedirectUri;

namespace Identity.Models.Entities.ClientPostLogoutRedirectUriEntity
{
    public class ClientPostLogoutRedirectUriObject
    {
        private readonly CreateClientPostLogoutRedirectUriRepoResult _createClientPostLogoutRedirectUriRepoResult;

        public ClientPostLogoutRedirectUriObject(
            CreateClientPostLogoutRedirectUriRepoResult createClientPostLogoutRedirectUriRepoResult)
        {
            _createClientPostLogoutRedirectUriRepoResult = createClientPostLogoutRedirectUriRepoResult;
        }

        public ClientPostLogoutRedirectUriGetCompleteApiModel GetComplete()
        {
            if (_createClientPostLogoutRedirectUriRepoResult == null)
            {
                throw new Exception("CreateClientPostLogoutRedirectUriRepoResult is null in GetComplete");

            }
            return new ClientPostLogoutRedirectUriGetCompleteApiModel(_createClientPostLogoutRedirectUriRepoResult);
        }
        public ClientPostLogoutRedirectUriGetSummaryApiModel GetSummary()
        {
            if (_createClientPostLogoutRedirectUriRepoResult == null)
            {
                throw new Exception("CreateClientPostLogoutRedirectUriRepoResult is null in GetSummary");

            }
            return new ClientPostLogoutRedirectUriGetSummaryApiModel(_createClientPostLogoutRedirectUriRepoResult);
        }
    }
}