using System;
using Identity.Models.ApiModels.ApiResourcesModels;
using Identity.Models.RepoResults.ApiResources;

namespace Identity.Models.Entities.ApiResourcesEntity
{
    public class ApiResourcesObject
    {
        private readonly CreateApiResourcesRepoResult _createApiResourcesRepoResult;

        public ApiResourcesObject(CreateApiResourcesRepoResult createApiResourcesRepoResult)
        {
            _createApiResourcesRepoResult = createApiResourcesRepoResult;
        }

        public ApiResourcesGetCompleteApiModel GetComplete()
        {
            if (_createApiResourcesRepoResult == null)
            {
                throw new Exception("CreateApiResourcesRepoResult is null in GetComplete");
            }
            return new ApiResourcesGetCompleteApiModel(_createApiResourcesRepoResult);
        }
        public ApiResourcesGetSummaryApiModel GetSummary()
        {
            if (_createApiResourcesRepoResult == null)
            {
                throw new Exception("CreateApiResourcesRepoResult is null in GetSummary");
            }
            return new ApiResourcesGetSummaryApiModel(_createApiResourcesRepoResult);
        }
    }
}