using System;
using Identity.Models.ApiModels.ClientClaimApiModels;
using Identity.Models.RepoResults.ClientClaim;

namespace Identity.Models.Entities.ClientClaimEntity
{
    public class ClientClaimObject
    {
        private readonly CreateClientClaimRepoResult _createClientClaimRepoResult;

        public ClientClaimObject(CreateClientClaimRepoResult createClientClaimRepoResult)
        {
            _createClientClaimRepoResult = createClientClaimRepoResult;
        }

        public ClientClaimGetCompleteApiModel GetComplete()
        {
            if (_createClientClaimRepoResult == null)
            {
                throw new Exception("CreateClientClaimRepoResult is null");
            }
            return new ClientClaimGetCompleteApiModel(_createClientClaimRepoResult);
        }
        public ClientClaimGetSummaryApiModel GetSummary()
        {
            if (_createClientClaimRepoResult == null)
            {
                throw new Exception("CreateClientClaimRepoResult is null");
            }
            return new ClientClaimGetSummaryApiModel(_createClientClaimRepoResult);
        }
        
    }
}