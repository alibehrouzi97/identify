using System;
using Identity.Models.ApiModels.ClientApiModels;
using Identity.Models.ApiModels.ClientRedirectUriApiModels;
using Identity.Models.RepoResults.ClientRedirectUri;

namespace Identity.Models.Entities.ClientRedirectUriEntity
{
    public class ClientRedirectUriObject
    {
        private readonly CreateClientRedirectUriRepoResult _createClientRedirectUriRepoResult;

        public ClientRedirectUriObject(CreateClientRedirectUriRepoResult createClientRedirectUriRepoResult)
        {
            _createClientRedirectUriRepoResult = createClientRedirectUriRepoResult;
            
        }

        public ClientRedirectUriGetCompleteApiModel GetComplete()
        {
            if (_createClientRedirectUriRepoResult == null)
            {
                
                throw new Exception("CreateClientRedirectUriRepoResult is null in GetComplete in ClientRedirectUriObject");
            }
            return new ClientRedirectUriGetCompleteApiModel(_createClientRedirectUriRepoResult);
        }
        
        public ClientRedirectUriGetSummaryApiModel GetSummary()
        {
            if (_createClientRedirectUriRepoResult == null)
            {
                throw new Exception("CreateClientRedirectUriRepoResult is null in GetSummary in ClientRedirectUriObject");
            }
            return new ClientRedirectUriGetSummaryApiModel(_createClientRedirectUriRepoResult);
        }
    }
}