using Identity.Models.ApiModels.ClientClaimApiModels;

namespace Identity.Models.RepoParams.ClientClaim
{
    public class UpdateClientClaimRepoParam
    {
        public UpdateClientClaimRepoParam(UpdateClientClaimApiModel clientClaimApiModel)
        {
            Type = clientClaimApiModel.Type;
            Value = clientClaimApiModel.Value;
            ClientId = clientClaimApiModel.ClientId;
        }
        public string Type { get; set; }
        public string Value { get; set; }
        public int ClientId { get; set; }
    }
}