using Identity.Models.ApiModels.ClientPostLogoutRedirectUriApiModuels;

namespace Identity.Models.RepoParams.ClientPostLogoutRedirectUri
{
    public class CreateClientPostLogoutRedirectUriRepoParam
    {
        public CreateClientPostLogoutRedirectUriRepoParam(
            CreateClientPostLogoutRedirectUriApiModel createClientPostLogoutRedirectUriApiModel)
        {
            PostLogoutRedirectUri = createClientPostLogoutRedirectUriApiModel.PostLogoutRedirectUri;
            ClientId = createClientPostLogoutRedirectUriApiModel.ClientId;
        }
        public string PostLogoutRedirectUri { get; set; }
        public int ClientId { get; set; }
    }
}