using Identity.Models.ApiModels.ClientScopeApiModels;

namespace Identity.Models.RepoParams.ClientScopeEntity
{
    public class CreateClientScopeRepoParam
    {
        public CreateClientScopeRepoParam(CreateClientScopeApiModel updateClientScopeApiModel)
        {
            ClientId = updateClientScopeApiModel.ClientId;
            Scope = updateClientScopeApiModel.Scope;
        }
        public string Scope { get; set; }
        public int ClientId { get; set; }
    }
}