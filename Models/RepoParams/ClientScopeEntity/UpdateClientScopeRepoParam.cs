using Identity.Models.ApiModels.ClientScopeApiModels;

namespace Identity.Models.RepoParams.ClientScopeEntity
{
    public class UpdateClientScopeRepoParam
    {
        public UpdateClientScopeRepoParam(UpdateClientScopeApiModel createClientScopeApiModel)
        {
            ClientId = createClientScopeApiModel.ClientId;
            Scope = createClientScopeApiModel.Scope;
        }
        public string Scope { get; set; }
        public int ClientId { get; set; }
    }
}