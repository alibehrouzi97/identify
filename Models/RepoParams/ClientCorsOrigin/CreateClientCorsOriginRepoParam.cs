using Identity.Models.ApiModels.ClientCorsOriginApiModel;

namespace Identity.Models.RepoParams.ClientCorsOrigin
{
    public class CreateClientCorsOriginRepoParam
    {
        public CreateClientCorsOriginRepoParam(CreateClientCorsOriginApiModel createClientCorsOriginApiModel)
        {
            Origin = createClientCorsOriginApiModel.Origin;
            ClientId = createClientCorsOriginApiModel.ClientId;
        }
        public string Origin { get; set; }
        public int ClientId { get; set; }
    }
}