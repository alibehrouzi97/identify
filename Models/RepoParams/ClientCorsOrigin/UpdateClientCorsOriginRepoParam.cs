using Identity.Models.ApiModels.ClientCorsOriginApiModel;

namespace Identity.Models.RepoParams.ClientCorsOrigin
{
    public class UpdateClientCorsOriginRepoParam
    {
        public UpdateClientCorsOriginRepoParam(UpdateClientCorsOriginApiModel updateClientCorsOriginApiModel)
        {
            Origin = updateClientCorsOriginApiModel.Origin;
            ClientId = updateClientCorsOriginApiModel.ClientId;
        }
        public string Origin { get; set; }
        public int ClientId { get; set; }
    }
}