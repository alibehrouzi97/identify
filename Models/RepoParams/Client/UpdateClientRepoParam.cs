using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Identity.Models.ApiModels.ClientApiModels;

namespace Identity.Models.RepoParams.Client
{
    public class UpdateClientRepoParam
    {
        public UpdateClientRepoParam(UpdateClientApiModel updateClientApiModel)
        {
            ClientName = updateClientApiModel.ClientName;
            Description = updateClientApiModel.Description;
            RequirePkce = updateClientApiModel.RequirePkce;
            AllowOfflineAccess = updateClientApiModel.AllowOfflineAccess;
            AlwaysSendClientClaims = updateClientApiModel.AlwaysSendClientClaims;
            LastAccessed = updateClientApiModel.LastAccessed;
            
            AllowedGrantTypes = updateClientApiModel.AllowedGrantTypes;
            RedirectUris = StringToList(updateClientApiModel.RedirectUris);
            PostLogoutRedirectUris = StringToList(updateClientApiModel.PostLogoutRedirectUris);
            AllowedScopes = StringToList(updateClientApiModel.AllowedScopes);
            Claims = StringToList(updateClientApiModel.Claims);
            AllowedCorsOrigins = StringToList(updateClientApiModel.AllowedCorsOrigins);
            
        }

        private static List<string> StringToList(string str)
        {
            if (str == null)
            {
                return new List<string>();

            }
            var arr = str.Split("\r\n");

            return arr.ToList();

        }
        public string ClientName { get; set; }
        public string Description { get; set; }
        
        public bool AllowOfflineAccess { get; set; }
        public bool RequirePkce { get; set; }
        public bool AlwaysSendClientClaims { get ; set; }
        
        public DateTime? LastAccessed { get ; set; }
        [Required]
        public List<string>  AllowedGrantTypes { get; set; }
        public List<string> RedirectUris { get ; set; }
        public List<string> PostLogoutRedirectUris { get ; set; }
        

        public List<string> AllowedScopes { get ; set; }
        public List<string> Claims { get ; set; }
       
        public List<string> AllowedCorsOrigins { get ; set; }
    }
}