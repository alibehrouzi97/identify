using System;
using System.Collections.Generic;
using System.Linq;
using Identity.Models.ApiModels.ClientApiModels;
using Identity.Models.Entities.ClientPostLogoutRedirectUriEntity;

namespace Identity.Models.RepoParams.Client
{
    public class CreateClientRepoParam
    {

        public CreateClientRepoParam(CreateClientApiModel createClientApiModel)
        {
            ClientName = createClientApiModel.ClientName;
            Description = createClientApiModel.Description;
            RequirePkce = createClientApiModel.RequirePkce;
            AllowOfflineAccess = createClientApiModel.AllowOfflineAccess;
            AlwaysSendClientClaims = createClientApiModel.AlwaysSendClientClaims;
            LastAccessed = createClientApiModel.LastAccessed;
            AllowedGrantTypes = createClientApiModel.AllowedGrantTypes;
            RedirectUris = StringToList(createClientApiModel.RedirectUris); 
            PostLogoutRedirectUris = StringToList(createClientApiModel.PostLogoutRedirectUris);
            AllowedScopes = StringToList(createClientApiModel.AllowedScopes);
            Claims = StringToList(createClientApiModel.Claims);
            AllowedCorsOrigins = StringToList(createClientApiModel.AllowedCorsOrigins);
            
            
        }

        private static List<string> StringToList(string str)
        {
            if (str == null)
            {
                return new List<string>();

            }
            var arr = str.Split("\r\n");

            return arr.ToList();

        }
        public string ClientName { get; set; }
        public string Description { get; set; }
        public List<string>  AllowedGrantTypes { get; set; }

        public bool RequirePkce { get; set; }
        
        public List<string> RedirectUris { get ; set; }
        public List<string> PostLogoutRedirectUris { get ; set; }
        public bool AllowOfflineAccess { get; set; }

        public List<string> AllowedScopes { get ; set; }
        public List< string> Claims { get ; set; }
        public bool AlwaysSendClientClaims { get ; set; }
        public List<string> AllowedCorsOrigins { get ; set; }
        public DateTime? LastAccessed { get ; set; }
    }
}