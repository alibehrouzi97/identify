using System;
using Identity.Models.ApiModels.ApiResourcesModels;

namespace Identity.Models.RepoParams.ApiResources
{
    public class UpdateApiResourcesRepoParam
    {
        public UpdateApiResourcesRepoParam(UpdateApiResourcesApiModel updateApiResourcesApiModel)
        {
            Name = updateApiResourcesApiModel.Name;
            DisplayName = updateApiResourcesApiModel.DisplayName;
            Description = updateApiResourcesApiModel.Description;
            Created = updateApiResourcesApiModel.Created;
        }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
    }
}