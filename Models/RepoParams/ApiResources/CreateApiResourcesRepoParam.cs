using Identity.Models.ApiModels.ApiResourcesModels;

namespace Identity.Models.RepoParams.ApiResources
{
    public class CreateApiResourcesRepoParam
    {
        public CreateApiResourcesRepoParam(CreateApiResourcesApiModel createApiResourcesApiModel)
        {
            Name = createApiResourcesApiModel.Name;
            Description = createApiResourcesApiModel.Description;
            DisplayName = createApiResourcesApiModel.DisplayName;
        }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
    }
}