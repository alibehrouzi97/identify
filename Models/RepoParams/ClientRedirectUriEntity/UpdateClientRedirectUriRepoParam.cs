using Identity.Models.ApiModels.ClientRedirectUriApiModels;

namespace Identity.Models.RepoParams.ClientRedirectUriEntity
{
    public class UpdateClientRedirectUriRepoParam
    {
        public UpdateClientRedirectUriRepoParam(UpdateClientRedirectUriApiModel updateClientRedirectUriApiModel)
        {
            RedirectUri = updateClientRedirectUriApiModel.RedirectUri;
            ClientId = updateClientRedirectUriApiModel.ClientId;
        }
        public string RedirectUri{ get; set; }
        public int ClientId{ get; set; }
    }
}