using Identity.Models.ApiModels.ClientRedirectUriApiModels;

namespace Identity.Models.RepoParams.ClientRedirectUriEntity
{
    public class CreateClientRedirectUriRepoParam
    {
        public CreateClientRedirectUriRepoParam(CreateClientRedirectUriApiModel createClientRedirectUriApiModel)
        {
            RedirectUri = createClientRedirectUriApiModel.RedirectUri;
            ClientId = createClientRedirectUriApiModel.ClientId;
        }
        public string RedirectUri{ get; set; }
        public int ClientId{ get; set; }
    }
}