using Identity.Models.ApiModels.ClientGrantTypeApiModels;

namespace Identity.Models.RepoParams.ClientGrantType
{
    public class UpdateClientGrantTypeRepoParam
    {
        public UpdateClientGrantTypeRepoParam(UpdateClientGrantTypeApiModel createClientGrantTypeApiModel)
        {
            GrantType = createClientGrantTypeApiModel.GrantType;
            ClientId = createClientGrantTypeApiModel.ClientId;
        }
        public string GrantType{ get; set; }
        public int ClientId{ get; set; }
    }
}