using Identity.Models.ApiModels.ClientApiModels;
using Identity.Models.ApiModels.ClientGrantTypeApiModels;

namespace Identity.Models.RepoParams.ClientGrantType
{
    public class CreateClientGrantTypeRepoParam
    {
        public CreateClientGrantTypeRepoParam(CreateClientGrantTypeApiModel createClientGrantTypeApiModel)
        {
            GrantType = createClientGrantTypeApiModel.GrantType;
            ClientId = createClientGrantTypeApiModel.ClientId;
        }
        public string GrantType{ get; set; }
        public int ClientId{ get; set; }
    }
}