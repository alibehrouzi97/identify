using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Identity.Models.Entities.ApiResourcesEntity;

namespace Identity.Data
{
    public class MvcIdentityContext : DbContext
    {
        public MvcIdentityContext (DbContextOptions<MvcIdentityContext> options)
            : base(options)
        {
        }

        public DbSet<Identity.Models.Entities.ApiResourcesEntity.ApiResources> ApiResources { get; set; }
    }
}
