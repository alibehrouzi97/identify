using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Identity.Data;
using Identity.Manager;
using Identity.Models.ApiModels.ApiResourcesModels;
using Identity.Models.Entities.ApiResourcesEntity;
using Microsoft.Extensions.Logging;

namespace Identity.Controllers
{
    public class ApiResourcesController : Controller
    {
        
        private readonly ILogger<ApiResourcesController> _logger;
        
        private readonly IApiResourcesManager _apiResourcesManager;

        public ApiResourcesController(ILogger<ApiResourcesController> logger,IApiResourcesManager apiResourcesManager)
        {
            _logger = logger;
            _apiResourcesManager = apiResourcesManager;

        }

        // GET: ApiResources
        public async Task<IActionResult> Index()
        {
            
            try
            {
                
                var apiResourcesObjects = await _apiResourcesManager.GetAllApiResourcesAsync();
                
                var apiResourcesGetSummaryApiModels = new List<ApiResourcesGetSummaryApiModel>();
                
                foreach (var apiResourcesObject in apiResourcesObjects)
                {
                    apiResourcesGetSummaryApiModels.Add(apiResourcesObject.GetSummary());
                }

                return View(apiResourcesGetSummaryApiModels);
            }
            
            catch(Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
        }

        // GET: ApiResources/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null,Details in ApiResourcesController");
                return BadRequest("id is null");
            }

            try
            {
                
                var apiResourcesObject = await _apiResourcesManager.GetOneApiResourcesAsync(id);
                
                return View(apiResourcesObject.GetComplete());
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
        }

        // GET: ApiResources/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ApiResources/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateApiResourcesApiModel createApiResourcesApiModel)
        {
            
            if (createApiResourcesApiModel == null)
            {
                _logger.LogError("createApiResourcesApiModel is null in Create in ApiResourcesController");
                return BadRequest("createApiResourcesApiModel is null");

            }
            
            if (ModelState.IsValid)
            {
                try
                {
                    
                    await _apiResourcesManager.CreateApiResourcesAsync(createApiResourcesApiModel);
                    
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return BadRequest(e.Message);
                }
            }
            
            return View(createApiResourcesApiModel);
        }

        // GET: ApiResources/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("ID is null ,Edit");
                return BadRequest("id is null");
            }

            try
            {
                
                var apiResourcesObject = await _apiResourcesManager.GetOneApiResourcesAsync(id);
                
                var apiResourcesGetCompleteApiModel = apiResourcesObject.GetComplete();
                
                var updateApiResourcesApiModel = new UpdateApiResourcesApiModel
                {
                    Name = apiResourcesGetCompleteApiModel.Name,
                    Description = apiResourcesGetCompleteApiModel.Description,
                    DisplayName = apiResourcesGetCompleteApiModel.DisplayName,
                };
            
                return View(updateApiResourcesApiModel);
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
        }

        // POST: ApiResources/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,UpdateApiResourcesApiModel updateApiResourcesApiModel)
        {
            
//            if (id==null)
//            {
//                _logger.LogError("ID is null ,Edit");
//                return BadRequest(new
//                {
//                    Status = "Error",
//                    Message = "id is null"
//                });
//                
//            }
            
            if (updateApiResourcesApiModel == null)
            {
                _logger.LogError("updateApiResourcesApiModel is null in Edit in ApiResourcesController");
                return BadRequest("updateApiResourcesApiModel is null");
            }
            
            if (ModelState.IsValid)
            {
                
                try
                {
                    await _apiResourcesManager.UpdateApiResourcesAsync(updateApiResourcesApiModel, id);
                }
                
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return BadRequest(e.Message);
                }
                
                return RedirectToAction(nameof(Index));
            }
            
            return View(updateApiResourcesApiModel);
            
        }

        // GET: ApiResources/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null,Delete ApiResourcesController");
                return BadRequest("id is null");
            }

            try
            {
                
                var apiResourcesObject = await _apiResourcesManager.GetOneApiResourcesAsync(id);

                return View(apiResourcesObject.GetComplete());
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }

        // POST: ApiResources/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            
//            if (id == null)
//            {
//                _logger.LogError("id is null,DeleteConfirmed ApiResourcesController");
//                return BadRequest("id is null");
//            }
            
            try
            {
                await _apiResourcesManager.DeleteApiResourcesAsync(id);
                return RedirectToAction(nameof(Index));
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }
        
    }
}
