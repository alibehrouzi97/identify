using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Identity.Manager;
using Identity.Models.ApiModels.ClientApiModels;
using Identity.Models.Entities.ClientClaimEntity;
using Identity.Models.Entities.ClientCorsOriginEntity;
using Identity.Models.Entities.ClientGrantTypeEntity;
using Identity.Models.Entities.ClientPostLogoutRedirectUriEntity;
using Identity.Models.Entities.ClientRedirectUriEntity;
using Identity.Models.Entities.ClientScopeEntity;
using Microsoft.Extensions.Logging;

namespace Identity.Controllers
{
    public class ClientController : Controller
    {
        
        private readonly ILogger<ClientController> _logger;
        
        private readonly IClientManager _clientManager;

        public ClientController(ILogger<ClientController> logger,IClientManager clientManager)
        {
            _logger = logger;
            _clientManager = clientManager;
        }

        // GET: Client
        public async Task<IActionResult> Index()
        {
            try
            {
                var clientObjects = await _clientManager.GetAllClientsAsync();
                
                var clientSummary=new List<ClientGetSummaryApiModel>();
                
                foreach (var clientObject in clientObjects)
                {
                    clientSummary.Add(clientObject.GetSummary());
                }
                
                return View(clientSummary);
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
            
        }

        // GET: Client/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null,Details in ClientController");
                return BadRequest("id is null");
            }

            try
            {
                var clientObject = await _clientManager.GetOneClientAsync(id);
                
                return View(clientObject.GetComplete());
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }


        }

        // GET: Client/CreateClientAsync
        public IActionResult CreateClientAsync()
        {
            return View();
        }

        // POST: Client/CreateClientAsync
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateClientAsync(CreateClientApiModel clientModel)
        {
            
            if (clientModel == null)
            {
                _logger.LogError("CreateClientApiModel is null in CreateClientAsync in ClientController");
                return BadRequest("clientModel is null");

            }
            
            if (ModelState.IsValid)
            {

                try
                {
                    await _clientManager.CreateClientAsync(clientModel);
                    return RedirectToAction(nameof(Index));
                    
                }
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return BadRequest(e.Message);
                }
                
                
            }
            
            return View(clientModel);
        }


        // GET: Client/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null");
                return BadRequest("id is null");
            }

            try
            {
                var client = await _clientManager.GetOneClientAsync(id);
                
                var clientGetComplete = client.GetComplete();
                
                var clientApiModel = new UpdateClientApiModel
                {
                    AllowedGrantTypes = GrantTypeListToStringList(clientGetComplete.AllowedGrantTypes),
                    Claims = ListToString(clientGetComplete.Claims),
                    PostLogoutRedirectUris = ListToString(clientGetComplete.PostLogoutRedirectUris),
                    AllowedCorsOrigins = ListToString(clientGetComplete.AllowedCorsOrigins),
                    AllowedScopes = ListToString(clientGetComplete.AllowedScopes),
                    RedirectUris = ListToString(clientGetComplete.RedirectUris),
                    ClientName = clientGetComplete.ClientName,
                    Description = clientGetComplete.Description,
                    LastAccessed = clientGetComplete.LastAccessed,
                    RequirePkce = clientGetComplete.RequirePkce,
                    AllowOfflineAccess = clientGetComplete.AllowOfflineAccess,
                    AlwaysSendClientClaims = clientGetComplete.AlwaysSendClientClaims
                };
            
                return View(clientApiModel);
                
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }

        }

        // POST: Client/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, UpdateClientApiModel updateClientApiModel)
        {

//            if (id==null)
//            {
//                _logger.LogError("ID is null ,Edit");
//                return BadRequest(new
//                {
//                    Status = "Error",
//                    Message = "id is null"
//                });
//                
//            }
            
            if (updateClientApiModel == null)
            {
                
                _logger.LogError("updateClientApiModel is null in Edit in Controller");
                
                return BadRequest("updateClientApiModel is null");
            }
            
            if (ModelState.IsValid)
            {
                
                try
                {
                    
                    await _clientManager.UpdateClientAsync(updateClientApiModel, id);
                    
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return BadRequest(e.Message);
                }
                
            }
            
            return RedirectToAction(nameof(Edit));
        }

        // GET: Client/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null,Delete ClientController");
                return BadRequest("id is null");
            }

            try
            {
                var client = await _clientManager.GetOneClientAsync(id);

                return View(client.GetComplete());
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }

        }

        // POST: Client/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            
//            if (id == null)
//            {
//                
//                _logger.LogError("id is null,DeleteConfirmed ClientController");
//                return BadRequest("id is null,DeleteConfirmed ClientController");
//
//            }

            try
            {
                await _clientManager.DeleteClientAsync(id);
                return RedirectToAction(nameof(Index));
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }
        public string ListToString(List<ClientRedirectUri> list)
        {
            
            string listToString = null;
            
            var flag = false;
            foreach (var lUri in list)
            {
                if (!flag)
                {
                    listToString += lUri.RedirectUri;
                    flag = true;
                }
                else
                {
                    listToString += "\r\n";
                    listToString += lUri.RedirectUri;
                }
            }

            return listToString;
        }
        public string ListToString(List<ClientScope> list)
        {
            string listToString = null;
            
            var flag = false;
            
            foreach (var lUri in list)
            {
                if (!flag)
                {
                    listToString += lUri.Scope;
                    flag = true;
                }
                else
                {
                    listToString += "\r\n";
                    listToString += lUri.Scope;
                }
            }

            return listToString;
        }
        
        public string ListToString(List<ClientClaim> list)
        {
            string listToString = null;
            
            var flag = false;
            foreach (var lUri in list)
            {
                
                if (!flag)
                {
                    listToString += lUri.Value;
                    flag = true;
                }
                
                else
                {
                    listToString += "\r\n";
                    listToString += lUri.Value;
                }
            }

            return listToString;
            
        }
        public string ListToString(List<ClientCorsOrigin> list)
        {
            string listToString = null;

            var flag = false;
            foreach (var lUri in list)
            {
                if (!flag)
                {
                    listToString += lUri.Origin;
                    flag = true;
                }
                else
                {
                    listToString += "\r\n";
                    listToString += lUri.Origin;
                }

            }

            return listToString;
            
        }
        public string ListToString(List<ClientPostLogoutRedirectUri> list)
        {
            string listToString = null;
            
            var flag = false;
            
            foreach (var lUri in list)
            {
                
                if (!flag)
                {
                    listToString += lUri.PostLogoutRedirectUri;
                    flag = true;
                }
                
                else
                {
                    listToString += "\r\n";
                    listToString += lUri.PostLogoutRedirectUri;
                }
            }

            return listToString;
            
        }
        public List<string> GrantTypeListToStringList(List<ClientGrantType> list)
        {
            var returnList=new List<string>();
            
            foreach (var lUri in list)
            {
                returnList.Add(lUri.GrantType);
            }

            return returnList;
        }


    }
}
