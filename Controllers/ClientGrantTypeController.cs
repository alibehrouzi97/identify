using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Identity.Data;
using Identity.Manager;
using Identity.Models.ApiModels.ClientGrantTypeApiModels;
using Identity.Models.Entities.ClientGrantTypeEntity;
using Microsoft.Extensions.Logging;

namespace Identity.Controllers
{
    public class ClientGrantTypeController : Controller
    {
        
        private readonly ILogger<ClientGrantTypeController> _logger;
        
        private readonly IClientGrantTypeManager _clientGrantTypeManager;

        public ClientGrantTypeController(ILogger<ClientGrantTypeController> logger,IClientGrantTypeManager clientGrantTypeManager )
        {

            _logger = logger;
            _clientGrantTypeManager = clientGrantTypeManager;
        }
        

        // GET: ClientGrantType
        public async Task<IActionResult> Index()
        {
            
            try
            {
                var clientGrantTypeObjects = await _clientGrantTypeManager.GetAllClientGrantTypesAsync();
                
                var grantTypeGetSummaryApiModels = new List<ClientGrantTypeGetSummaryApiModel>();
                
                foreach (var clientGrantTypeObject in clientGrantTypeObjects)
                {
                    grantTypeGetSummaryApiModels.Add(clientGrantTypeObject.GetSummary());
                }

                return View(grantTypeGetSummaryApiModels);
            }
            catch(Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
        }

        // GET: ClientGrantType/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null,Details in ClientGrantTypeController");
                return BadRequest("id is null");
            }

            try
            {
                var clientCorsOriginObject = await _clientGrantTypeManager.GetOneClientGrantTypeAsync(id);
                
                return View(clientCorsOriginObject.GetComplete());
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
        }

        // GET: ClientGrantType/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ClientGrantType/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateClientGrantTypeApiModel createClientGrantTypeApiModel)
        {
            
            if (createClientGrantTypeApiModel == null)
            {
                _logger.LogError("CreateClientGrantTypeApiModel is null in Create in ClientGrantTypeController");
                return BadRequest("createClientGrantTypeApiModel is null");

            }
            
            if (ModelState.IsValid)
            {
                
                try
                {
                    await _clientGrantTypeManager.CreateClientGrantTypeAsync(createClientGrantTypeApiModel);
                    return RedirectToAction(nameof(Index));
                }
                
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return BadRequest(e.Message);
                }

            }
            
            return View(createClientGrantTypeApiModel);
        }

        // GET: ClientGrantType/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null,Edit");
                return BadRequest("id is null");
            }

            try
            {
                var clientCorsOriginObject = await _clientGrantTypeManager.GetOneClientGrantTypeAsync(id);
                
                var corsOriginGetCompleteApiModel = clientCorsOriginObject.GetComplete();
                
                var updateClientCorsOriginApiModel = new UpdateClientGrantTypeApiModel
                {
                    ClientId = corsOriginGetCompleteApiModel.ClientId,
                    GrantType = corsOriginGetCompleteApiModel.GrantType
                    
                };
            
                return View(updateClientCorsOriginApiModel);
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            
        }

        // POST: ClientGrantType/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, UpdateClientGrantTypeApiModel updateClientGrantTypeApiModel)
        {
            
//            if (id==null)
//            {
//                _logger.LogError("ID is null ,Edit");
//                
//                return BadRequest("id is null");
//                
//            }
            
            if (updateClientGrantTypeApiModel == null)
            {
                _logger.LogError("UpdateClientGrantTypeApiModel is null in Edit in ClientGrantTypeController");
                
                return BadRequest("updateClientGrantTypeApiModel is null");
            }
            
            if (ModelState.IsValid)
            {
                
                try
                {
                    await _clientGrantTypeManager.UpdateClientGrantTypeAsync(updateClientGrantTypeApiModel, id);
                }
                
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return BadRequest(e.Message);
                }
                
                return RedirectToAction(nameof(Index));
            }
            
            return View(updateClientGrantTypeApiModel);
            
        }

        // GET: ClientGrantType/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null,Delete ClientGrantTypeController");
                return BadRequest("id is null");
            }

            try
            {
                var clientGrantTypeObject = await _clientGrantTypeManager.GetOneClientGrantTypeAsync(id);

                return View(clientGrantTypeObject.GetComplete());
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }

        // POST: ClientGrantType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            
//            if (id == null)
//            {
//                _logger.LogError("id is null,DeleteConfirmed ClientGrantTypeController");
//                return BadRequest("id is null");
//            }
//            
            try
            {
                await _clientGrantTypeManager.DeleteClientGrantTypeAsync(id);
                return RedirectToAction(nameof(Index));
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }
        
    }
}
