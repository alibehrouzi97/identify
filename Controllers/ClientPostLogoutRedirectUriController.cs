using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Identity.Data;
using Identity.Manager;
using Identity.Models.ApiModels.ClientPostLogoutRedirectUriApiModuels;
using Identity.Models.Entities.ClientPostLogoutRedirectUriEntity;
using Microsoft.Extensions.Logging;

namespace Identity.Controllers
{
    public class ClientPostLogoutRedirectUriController : Controller
    {
     
        private readonly ILogger<ClientPostLogoutRedirectUriController> _logger;
        
        private readonly IClientPostLogoutRedirectUriManager _clientPostLogoutRedirectUriManager;

        public ClientPostLogoutRedirectUriController(ILogger<ClientPostLogoutRedirectUriController> logger,IClientPostLogoutRedirectUriManager clientPostLogoutRedirectUriManager)
        {
            _logger = logger;
            _clientPostLogoutRedirectUriManager = clientPostLogoutRedirectUriManager;
            
        }

        // GET: ClientPostLogoutRedirectUri
        public async Task<IActionResult> Index()
        {
            
            try
            {
                var clientPostLogoutRedirectUriObjects = await _clientPostLogoutRedirectUriManager.GetAllClientPostLogoutRedirectUrisAsync();
                
                var clientPostLogoutRedirectUriGetSummaryApiModels = new List<ClientPostLogoutRedirectUriGetSummaryApiModel>();
                
                foreach (var clientPostLogoutRedirectUriObject in clientPostLogoutRedirectUriObjects)
                {
                    clientPostLogoutRedirectUriGetSummaryApiModels.Add(clientPostLogoutRedirectUriObject.GetSummary());
                }

                return View(clientPostLogoutRedirectUriGetSummaryApiModels);
            }
            catch(Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
        }

        // GET: ClientPostLogoutRedirectUri/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null,Details in ClientPostLogoutRedirectUriController");
                return BadRequest("id is null");
            }

            try
            {
                var clientPostLogoutRedirectUriObject = await _clientPostLogoutRedirectUriManager.GetOneClientPostLogoutRedirectUriAsync(id);
                
                return View(clientPostLogoutRedirectUriObject.GetComplete());
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }

        // GET: ClientPostLogoutRedirectUri/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ClientPostLogoutRedirectUri/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateClientPostLogoutRedirectUriApiModel createClientPostLogoutRedirectUriApiModel)
        {
            
            if (createClientPostLogoutRedirectUriApiModel == null)
            {
                _logger.LogError("CreateClientPostLogoutRedirectUriApiModel is null in Create in ClientPostLogoutRedirectUriController");
                
                return BadRequest("createClientPostLogoutRedirectUriApiModel is null");

            }
            
            if (ModelState.IsValid)
            {
                
                try
                {
                    await _clientPostLogoutRedirectUriManager.CreateClientPostLogoutRedirectUriAsync(createClientPostLogoutRedirectUriApiModel);
                    
                    return RedirectToAction(nameof(Index));
                }
                
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return BadRequest(e.Message);
                }
            }
            
            return View(createClientPostLogoutRedirectUriApiModel);
        }

        // GET: ClientPostLogoutRedirectUri/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null");
                return BadRequest("id is null");
            }

            try
            {
                var clientPostLogoutRedirectUriObject = await _clientPostLogoutRedirectUriManager.GetOneClientPostLogoutRedirectUriAsync(id);
                
                var clientPostLogoutRedirectUriGetCompleteApiModel = clientPostLogoutRedirectUriObject.GetComplete();
                
                var updateClientPostLogoutRedirectUriApiModel = new UpdateClientPostLogoutRedirectUriApiModel
                {
                    ClientId = clientPostLogoutRedirectUriGetCompleteApiModel.ClientId,
                    PostLogoutRedirectUri = clientPostLogoutRedirectUriGetCompleteApiModel.PostLogoutRedirectUri
                    
                };
            
                return View(updateClientPostLogoutRedirectUriApiModel);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }

        // POST: ClientPostLogoutRedirectUri/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,UpdateClientPostLogoutRedirectUriApiModel updateClientPostLogoutRedirectUriApiModel)
        {
            
//            if (id==null)
//            {
//                _logger.LogError("ID is null ,Edit");
//                return BadRequest("id is null");
//                
//            }
            
            if (updateClientPostLogoutRedirectUriApiModel == null)
            {
                
                _logger.LogError("updateClientPostLogoutRedirectUriApiModel is null in Edit in ClientPostLogoutRedirectUriController");
                return BadRequest("updateClientPostLogoutRedirectUriApiModel is null");
            }
            
            if (ModelState.IsValid)
            {
                
                try
                {
                    await _clientPostLogoutRedirectUriManager.UpdateClientPostLogoutRedirectUriAsync(updateClientPostLogoutRedirectUriApiModel, id);
                }
                
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return BadRequest(e.Message);
                }
                
                return RedirectToAction(nameof(Index));
            }
            
            return View(updateClientPostLogoutRedirectUriApiModel);
            
        }

        // GET: ClientPostLogoutRedirectUri/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null,Delete ClientPostLogoutRedirectUriController");
                return BadRequest("id is null");
            }

            try
            {
                var clientPostLogoutRedirectUriObject = await _clientPostLogoutRedirectUriManager.GetOneClientPostLogoutRedirectUriAsync(id);

                return View(clientPostLogoutRedirectUriObject.GetComplete());
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }

        // POST: ClientPostLogoutRedirectUri/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            
//            if (id == null)
//            {
//                _logger.LogError("id is null,DeleteConfirmed ClientPostLogoutRedirectUriController");
//                return BadRequest("id is null,DeleteConfirmed ClientPostLogoutRedirectUriController");
//            }
            
            try
            {
                await _clientPostLogoutRedirectUriManager.DeleteClientPostLogoutRedirectUriAsync(id);
                return RedirectToAction(nameof(Index));
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
        }
    }
}
