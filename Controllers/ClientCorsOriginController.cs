using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Identity.Data;
using Identity.Manager;
using Identity.Models.ApiModels.ClientCorsOriginApiModel;
using Identity.Models.Entities.ClientCorsOriginEntity;
using Microsoft.Extensions.Logging;

namespace Identity.Controllers
{
    public class ClientCorsOriginController : Controller
    {
        
        private readonly ILogger<ClientCorsOriginController> _logger;
        
        private readonly IClientCorsOriginManager _clientCorsOriginManager;

        public ClientCorsOriginController(ILogger<ClientCorsOriginController> logger,IClientCorsOriginManager clientCorsOriginManager)
        {
            _clientCorsOriginManager = clientCorsOriginManager;
            _logger = logger;
        }

        // GET: ClientCorsOrigin
        public async Task<IActionResult> Index()
        {
            try
            {
                var allClientCorsOriginObjects = await _clientCorsOriginManager.GetAllClientCorsOriginAsync();
                
                var corsOriginGetSummaryApiModels = new List<ClientCorsOriginGetSummaryApiModel>();
                
                foreach (var clientCorsOriginObject in allClientCorsOriginObjects)
                {
                    corsOriginGetSummaryApiModels.Add(clientCorsOriginObject.GetSummary());
                }

                return View(corsOriginGetSummaryApiModels);
            }
            catch(Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
        }

        // GET: ClientCorsOrigin/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id.ToString() == null)
            {
                _logger.LogError("id is null,Details in ClientCorsOriginController");
                return BadRequest("id is null");
            }

            try
            {
                var clientCorsOriginObject = await _clientCorsOriginManager.GetOneClientCorsOriginAsync(id);
                
                return View(clientCorsOriginObject.GetComplete());
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
        }

        // GET: ClientCorsOrigin/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ClientCorsOrigin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateClientCorsOriginApiModel clientCorsOriginApiModel)
        {
            
            if (clientCorsOriginApiModel == null)
            {
                
                _logger.LogError("CreateClientCorsOriginApiModel is null in Create in ClientCorsOriginController");
                
                return BadRequest("clientCorsOriginApiModel is null");

            }
            
            if (ModelState.IsValid)
            {
                
                try
                {
                    await _clientCorsOriginManager.CreateClientCorsOriginAsync(clientCorsOriginApiModel);
                    
                    return RedirectToAction(nameof(Index));
                }
                
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return BadRequest(e.Message);
                }
            }
            
            return View(clientCorsOriginApiModel);
        }

        // GET: ClientCorsOrigin/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null");
                return BadRequest("id is null");
            }

            try
            {
                
                var clientCorsOriginObject = await _clientCorsOriginManager.GetOneClientCorsOriginAsync(id);
                
                var corsOriginGetCompleteApiModel = clientCorsOriginObject.GetComplete();
                
                var updateClientCorsOriginApiModel = new UpdateClientCorsOriginApiModel
                {
                    ClientId = corsOriginGetCompleteApiModel.ClientId,
                    Origin = corsOriginGetCompleteApiModel.Origin
                    
                };
            
                return View(updateClientCorsOriginApiModel);
                
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
        }

        // POST: ClientCorsOrigin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, UpdateClientCorsOriginApiModel updateClientCorsOriginApiModel)
        {
            
            if (id.ToString()==null)
            {
                _logger.LogError("ID is null ,Edit");
                
                return BadRequest("id is null");
                
            }
            
            if (updateClientCorsOriginApiModel == null)
            {
                
                _logger.LogError("UpdateClientCorsOriginApiModel is null in Edit in Controller");
                
                return BadRequest("updateClientCorsOriginApiModel is null");
            }
            
            if (ModelState.IsValid)
            {
                
                try
                {
                    await _clientCorsOriginManager.UpdateClientCorsOriginAsync(updateClientCorsOriginApiModel, id);
                }
                
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return BadRequest(e.Message);
                }
                
                return RedirectToAction(nameof(Index));
                
            }
            
            return View(updateClientCorsOriginApiModel);
            
        }

        // GET: ClientCorsOrigin/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null,Delete ClientCorsOriginController");
                return BadRequest("id is null");
            }

            try
            {
                var clientCorsOriginObject = await _clientCorsOriginManager.GetOneClientCorsOriginAsync(id);
                
                return View(clientCorsOriginObject.GetComplete());
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
        }

        // POST: ClientCorsOrigin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            
//            if (id == null)
//            {
//                _logger.LogError("id is null,DeleteConfirmed ClientCorsOriginController");
//                return NotFound("id is null,DeleteConfirmed ClientCorsOriginController");
//            }
            
            try
            {
                await _clientCorsOriginManager.DeleteClientCorsOriginAsync(id);
                
                return RedirectToAction(nameof(Index));
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }
    }
}
