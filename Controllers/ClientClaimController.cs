using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Identity.Data;
using Identity.Manager;
using Identity.Models.ApiModels.ClientClaimApiModels;
using Identity.Models.Entities.ClientClaimEntity;
using Microsoft.Extensions.Logging;

namespace Identity.Controllers
{
    public class ClientClaimController : Controller
    {
        
        private readonly ILogger<ClientController> _logger;
        
        private readonly IClientClaimManager _clientClaimManager;

        public ClientClaimController(ILogger<ClientController> logger,IClientClaimManager clientClaimManager)
        {
            _logger = logger;
            _clientClaimManager = clientClaimManager;
            
        }

        // GET: ClientClaim
        public async Task<IActionResult> Index()
        {
            
            try
            {
                var clientClaimObjects = await _clientClaimManager.GetAllClientClaimsAsync();
                
                var clientClaimSummary = new List<ClientClaimGetSummaryApiModel>();
                
                foreach (var clientClaimObject in clientClaimObjects)
                {
                    clientClaimSummary.Add(clientClaimObject.GetSummary());
                }

                return View(clientClaimSummary);
            }
            
            catch(Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
        }

        // GET: ClientClaim/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null,Details in ClientClaimController");
                return BadRequest("id is null");
            }

            try
            {
                var clientClaimObject = await _clientClaimManager.GetOneClientClaimAsync(id);
                
                return View(clientClaimObject.GetComplete());
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
        }

        // GET: ClientClaim/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ClientClaim/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateClientClaimApiModel clientClaimApiModel)
        {
            
            if (clientClaimApiModel == null)
            {
                _logger.LogError("CreateClientClaimApiModel is null in Create in ClientClaimController");
                return BadRequest("clientClaimApiModel is null");

            }
            
            if (ModelState.IsValid)
            {

                try
                {
                    await _clientClaimManager.CreateClientClaimAsync(clientClaimApiModel);
                    
                    return RedirectToAction(nameof(Index));
                    
                }
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return BadRequest(e.Message);
                }
            }
            
            return View(clientClaimApiModel);
            
        }

        // GET: ClientClaim/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null");
                return BadRequest("id is null");
            }

            try
            {
                var clientClaimObject = await _clientClaimManager.GetOneClientClaimAsync(id);
                
                var clientClaimGetComplete = clientClaimObject.GetComplete();
                
                var clientClaimApiModel = new UpdateClientClaimApiModel
                {
                    ClientId = clientClaimGetComplete.ClientId,
                    Type = clientClaimGetComplete.Type,
                    Value = clientClaimGetComplete.Value
                };
                
            
                return View(clientClaimApiModel);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
        }

        // POST: ClientClaim/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, UpdateClientClaimApiModel updateClientClaimApiModel)
        {

//            if (id==null)
//            {
//                _logger.LogError("ID is null ,Edit");
//                return BadRequest(new
//                {
//                    Status = "Error",
//                    Message = "id is null"
//                });
//                
//            }
            
            if (updateClientClaimApiModel == null)
            {
                _logger.LogError("UpdateClientClaimApiModel is null in Edit in Controller");
                return BadRequest("updateClientClaimApiModel is null");
            }
            
            if (ModelState.IsValid)
            {
                
                try
                {
                    await _clientClaimManager.UpdateClientClaimAsync(updateClientClaimApiModel, id);
                }
                
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return BadRequest(e.Message);
                }
                
                return RedirectToAction(nameof(Index));
            }
            
            return View(updateClientClaimApiModel);
        }

        // GET: ClientClaim/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null,Delete ClientClaimController");
                return BadRequest("id is null,Delete ClientClaimController");
            }

            try
            {
                var clientClaim = await _clientClaimManager.GetOneClientClaimAsync(id);
                
                return View(clientClaim.GetComplete());
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }

        // POST: ClientClaim/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            
//            if (id == null)
//            {
//                _logger.LogError("id is null,DeleteConfirmed ClientClaimController");
//                return BadRequest("id is null,DeleteConfirmed ClientClaimController");
//
//            }

            try
            {
                await _clientClaimManager.DeleteClientClaimAsync(id);
                return RedirectToAction(nameof(Index));
                
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
        }
    }
}
