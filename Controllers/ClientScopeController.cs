using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Identity.Data;
using Identity.Manager;
using Identity.Models.ApiModels.ClientScopeApiModels;
using Identity.Models.Entities.ClientScopeEntity;
using Microsoft.Extensions.Logging;

namespace Identity.Controllers
{
    public class ClientScopeController : Controller
    {

        private readonly ILogger<ClientRedirectUriController> _logger;
        
        private readonly IClientScopeManager _clientScopeManager;
        
        public ClientScopeController(ILogger<ClientRedirectUriController> logger,IClientScopeManager clientScopeManager)
        {
            _logger = logger;
            _clientScopeManager = clientScopeManager;
        }
        

        // GET: ClientScope
        public async Task<IActionResult> Index()
        {
            try
            {
                
                var clientScopeObjects = await _clientScopeManager.GetAllClientScopesAsync();
                
                var clientScopeGetSummaryApiModels = new List<ClientScopeGetSummaryApiModel>();
                
                foreach (var clientScopeObject in clientScopeObjects)
                {
                    clientScopeGetSummaryApiModels.Add(clientScopeObject.GetSummary());
                }

                return View(clientScopeGetSummaryApiModels);
                
            }
            
            catch(Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }

        // GET: ClientScope/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null,Details in ClientScopeController");
                return BadRequest("id is null");
            }

            try
            {
                var clientScopeObject = await _clientScopeManager.GetOneClientScopeAsync(id);
                
                return View(clientScopeObject.GetComplete());
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }

        // GET: ClientScope/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ClientScope/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateClientScopeApiModel createClientScopeApiModel)
        {
            
            if (createClientScopeApiModel == null)
            {
                _logger.LogError("createClientScopeApiModel is null in Create in ClientScopeController");
                return BadRequest("createClientScopeApiModel is null");

            }
            
            if (ModelState.IsValid)
            {
                
                try
                {
                    await _clientScopeManager.CreateClientScopeAsync(createClientScopeApiModel);
                    
                    return RedirectToAction(nameof(Index));
                }
                
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return BadRequest(e.Message);
                }
                
            }
            
            return View(createClientScopeApiModel);
            
        }

        // GET: ClientScope/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null");
                return BadRequest("id is null");
            }

            try
            {
                
                var clientRedirectUriObject = await _clientScopeManager.GetOneClientScopeAsync(id);
                
                var clientRedirectUriGetCompleteApiModel = clientRedirectUriObject.GetComplete();
                
                var updateClientRedirectUriApiModel = new UpdateClientScopeApiModel
                {
                    ClientId = clientRedirectUriGetCompleteApiModel.ClientId,
                    Scope = clientRedirectUriGetCompleteApiModel.Scope
                    
                };
            
                return View(updateClientRedirectUriApiModel);
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }

        // POST: ClientScope/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,UpdateClientScopeApiModel updateClientScopeApiModel)
        {
            
//            if (id==null)
//            {
//                _logger.LogError("ID is null ,Edit");
//                
//                return BadRequest( "id is null");
//                
//            }
            
            if (updateClientScopeApiModel == null)
            {
                
                _logger.LogError("updateClientScopeApiModel is null in Edit in ClientScopeController");
                
                return BadRequest("updateClientScopeApiModel is null");
                
            }
            
            if (ModelState.IsValid)
            {
                
                try
                {
                    await _clientScopeManager.UpdateClientScopeAsync(updateClientScopeApiModel, id);
                }
                
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return BadRequest(e.Message);
                }
                
                return RedirectToAction(nameof(Index));
                
            }
            
            return View(updateClientScopeApiModel);
            
        }

        // GET: ClientScope/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null,Delete ClientScopeController");
                return BadRequest("id is null");
            }

            try
            {
                var clientScopeObject = await _clientScopeManager.GetOneClientScopeAsync(id);

                return View(clientScopeObject.GetComplete());
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }

        // POST: ClientScope/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            
//            if (id == null)
//            {
//                _logger.LogError("id is null,DeleteConfirmed ClientScopeController");
//                return BadRequest("id is null,DeleteConfirmed ClientScopeController");
//            }
            
            try
            {
                await _clientScopeManager.DeleteClientScopeAsync(id);
                return RedirectToAction(nameof(Index));
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }
    }
}
