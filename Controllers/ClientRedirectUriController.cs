using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Identity.Data;
using Identity.Manager;
using Identity.Models.ApiModels.ClientRedirectUriApiModels;
using Identity.Models.Entities.ClientRedirectUriEntity;
using Microsoft.Extensions.Logging;

namespace Identity.Controllers
{
    public class ClientRedirectUriController : Controller
    {

        private readonly ILogger<ClientRedirectUriController> _logger;
        
        private readonly IClientRedirectUriManager _clientRedirectUriManager;

        public ClientRedirectUriController(ILogger<ClientRedirectUriController> logger,IClientRedirectUriManager clientRedirectUriManager)
        {
            
            _logger = logger;
            _clientRedirectUriManager = clientRedirectUriManager;

        }

        // GET: ClientRedirectUri
        public async Task<IActionResult> Index()
        {
            
            try
            {
                
                var clientRedirectUriObjects = await _clientRedirectUriManager.GetAllClientRedirectUrisAsync();
                
                var clientRedirectUriGetSummaryApiModels = new List<ClientRedirectUriGetSummaryApiModel>();
                
                foreach (var clientRedirectUriObject in clientRedirectUriObjects)
                {
                    clientRedirectUriGetSummaryApiModels.Add(clientRedirectUriObject.GetSummary());
                }

                return View(clientRedirectUriGetSummaryApiModels);
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }

        // GET: ClientRedirectUri/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null,Details in ClientRedirectUriController");
                return BadRequest("id is null");
            }

            try
            {
                var clientRedirectUriObject = await _clientRedirectUriManager.GetOneClientRedirectUriAsync(id);
                
                return View(clientRedirectUriObject.GetComplete());
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }

        // GET: ClientRedirectUri/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ClientRedirectUri/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateClientRedirectUriApiModel createClientRedirectUriApiModel)
        {
            
            if (createClientRedirectUriApiModel == null)
            {
                _logger.LogError("createClientRedirectUriApiModel is null in Create in ClientRedirectUriController");
                return BadRequest( "createClientRedirectUriApiModel is null");

            }
            
            if (ModelState.IsValid)
            {
                try
                {
                    await _clientRedirectUriManager.CreateClientRedirectUriAsync(createClientRedirectUriApiModel);
                    
                    return RedirectToAction(nameof(Index));
                }
                
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return BadRequest(e.Message);
                }
                
            }
            
            return View(createClientRedirectUriApiModel);
        }

        // GET: ClientRedirectUri/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null");
                return BadRequest("id is null");
            }

            try
            {
                var clientRedirectUriObject = await _clientRedirectUriManager.GetOneClientRedirectUriAsync(id);
                
                var clientRedirectUriGetCompleteApiModel = clientRedirectUriObject.GetComplete();
                
                var updateClientRedirectUriApiModel = new UpdateClientRedirectUriApiModel
                {
                    ClientId = clientRedirectUriGetCompleteApiModel.ClientId,
                    RedirectUri = clientRedirectUriGetCompleteApiModel.RedirectUri
                    
                };
            
                return View(updateClientRedirectUriApiModel);
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }

        // POST: ClientRedirectUri/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,UpdateClientRedirectUriApiModel updateClientRedirectUriApiModel)
        {
            
//            if (id==null)
//            {
//                _logger.LogError("ID is null ,Edit");
//                return BadRequest("id is null");
//                
//            }
            
            if (updateClientRedirectUriApiModel == null)
            {
                _logger.LogError("updateClientRedirectUriApiModel is null in Edit in ClientRedirectUriController");
                return BadRequest("updateClientRedirectUriApiModel is null");
            }
            
            if (ModelState.IsValid)
            {
                
                try
                {
                    await _clientRedirectUriManager.UpdateClientRedirectUriAsync(updateClientRedirectUriApiModel, id);
                }
                
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return BadRequest(e.Message);
                }
                
                return RedirectToAction(nameof(Index));
                
            }
            
            return View(updateClientRedirectUriApiModel);
            
        }

        // GET: ClientRedirectUri/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            
            if (id == null)
            {
                _logger.LogError("id is null,Delete ClientRedirectUriController");
                return BadRequest("id is null");
            }

            try
            {
                var clientRedirectUriObject = await _clientRedirectUriManager.GetOneClientRedirectUriAsync(id);

                return View(clientRedirectUriObject.GetComplete());
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }

        // POST: ClientRedirectUri/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            
//            if (id == null)
//            {
//                _logger.LogError("id is null,DeleteConfirmed ClientRedirectUriController");
//                return BadRequest("id is null,DeleteConfirmed ClientRedirectUriController");
//            }
            
            try
            {
                await _clientRedirectUriManager.DeleteClientRedirectUriAsync(id);
                
                return RedirectToAction(nameof(Index));
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            
        }

    }
}
