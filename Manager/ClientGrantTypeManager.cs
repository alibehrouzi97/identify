using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Models.ApiModels.ClientGrantTypeApiModels;
using Identity.Models.Entities.ClientGrantTypeEntity;
using Identity.Models.RepoParams.ClientGrantType;
using Identity.Repository;
using Microsoft.Extensions.Logging;

namespace Identity.Manager
{
    public interface IClientGrantTypeManager
    {
        Task DeleteClientGrantTypeAsync(int id);
        Task<ClientGrantTypeObject> GetOneClientGrantTypeAsync(int? id);
        Task<List<ClientGrantTypeObject>> GetAllClientGrantTypesAsync();
        Task UpdateClientGrantTypeAsync(UpdateClientGrantTypeApiModel updateClientGrantTypeApiModel, int id);
        Task CreateClientGrantTypeAsync(CreateClientGrantTypeApiModel createClientGrantTypeApi);


    }
    public class ClientGrantTypeManager : IClientGrantTypeManager
    {
        
        private readonly ILogger<ClientCorsOriginManager> _logger;
        
        private readonly IClientGrantTypeRepository _clientGrantTypeRepository;
        
        public ClientGrantTypeManager(ILogger<ClientCorsOriginManager> logger, IClientGrantTypeRepository clientGrantTypeRepository)
        {
            _logger = logger;
            _clientGrantTypeRepository = clientGrantTypeRepository;
        }
        
        public async Task CreateClientGrantTypeAsync(CreateClientGrantTypeApiModel createClientGrantTypeApi)
        {
            
            if (createClientGrantTypeApi==null)
            {
                _logger.LogError("CreateClientGrantTypeApiModel is null , CreateClientGrantTypeAsync in ClientGrantTypeManager");
                throw new Exception("CreateClientGrantTypeApiModel is null , CreateClientGrantTypeAsync in ClientGrantTypeManager");
            }

            var createClientGrantTypeRepoParam = new CreateClientGrantTypeRepoParam(createClientGrantTypeApi);

            try
            {
                await _clientGrantTypeRepository.CreateClientGrantTypeAsync(createClientGrantTypeRepoParam);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task UpdateClientGrantTypeAsync(UpdateClientGrantTypeApiModel updateClientGrantTypeApiModel,int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null,UpdateClientGrantTypeAsync ClientGrantTypeManager");
                throw new Exception("id is null,UpdateClientGrantTypeAsync ClientGrantTypeManager");

            }
            
            if (updateClientGrantTypeApiModel == null)
            {
                _logger.LogError("in UpdateClientGrantTypeAsync ClientGrantTypeManager ,UpdateClientGrantTypeApiModel is null");
                throw new Exception("UpdateClientGrantTypeApiModel is null");

            }

            var updateClientGrantTypeRepoParam = new UpdateClientGrantTypeRepoParam(updateClientGrantTypeApiModel);
            try
            {
                await _clientGrantTypeRepository.UpdateClientGrantTypeAsync(updateClientGrantTypeRepoParam,id);
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task<List<ClientGrantTypeObject>> GetAllClientGrantTypesAsync()
        {
            
            try
            {
                
                var grantTypeRepoResults = await _clientGrantTypeRepository.GetAllClientGrantTypesAsync();
                
                var clientGrantTypeObjects=new List<ClientGrantTypeObject>();
                
                foreach (var grantTypeRepoResult in grantTypeRepoResults)
                {
                    clientGrantTypeObjects.Add(new ClientGrantTypeObject(grantTypeRepoResult));
                }

                return clientGrantTypeObjects;
                
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task<ClientGrantTypeObject> GetOneClientGrantTypeAsync(int? id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, GetOneClientGrantTypeAsync ClientGrantTypeManager");
                throw new Exception("id is null, GetOneClientGrantTypeAsync ClientGrantTypeManager");
            }

            try
            {
                var clientGrantTypeRepoResult = await _clientGrantTypeRepository.GetOneClientGrantTypeAsync(id);
                
                return new ClientGrantTypeObject(clientGrantTypeRepoResult);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task DeleteClientGrantTypeAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("Id is null,DeleteClientGrantTypeAsync ClientGrantTypeManager");
                throw new Exception("ID is null in DeleteClientGrantTypeAsync in ClientGrantTypeManager");
            }

            try
            {
                await _clientGrantTypeRepository.DeleteClientGrantTypeAsync(id);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }
    }
}