using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Models.ApiModels.ClientCorsOriginApiModel;
using Identity.Models.Entities.ClientClaimEntity;
using Identity.Models.Entities.ClientCorsOriginEntity;
using Identity.Models.RepoParams.ClientCorsOrigin;
using Identity.Repository;
using Microsoft.Extensions.Logging;

namespace Identity.Manager
{
    public interface IClientCorsOriginManager
    {
        Task DeleteClientCorsOriginAsync(int id);
        Task<ClientCorsOriginObject> GetOneClientCorsOriginAsync(int? id);
        Task<List<ClientCorsOriginObject>> GetAllClientCorsOriginAsync();
        Task UpdateClientCorsOriginAsync(UpdateClientCorsOriginApiModel clientCorsOriginApiModel, int id);
        Task CreateClientCorsOriginAsync(CreateClientCorsOriginApiModel createClientCorsOriginApiModel);

    }
    public class ClientCorsOriginManager : IClientCorsOriginManager
    {
        
        private readonly ILogger<ClientCorsOriginManager> _logger;
        
        private readonly IClientCorsOriginRepository _clientCorsOriginRepository;

        public ClientCorsOriginManager(ILogger<ClientCorsOriginManager> logger, IClientCorsOriginRepository clientCorsOriginRepository)
        {
            _logger = logger;
            _clientCorsOriginRepository = clientCorsOriginRepository;
            
        }
        
        public async Task CreateClientCorsOriginAsync(CreateClientCorsOriginApiModel createClientCorsOriginApiModel)
        {
            
            if (createClientCorsOriginApiModel==null)
            {
                _logger.LogError("CreateClientCorsOriginApiModel is null , CreateClientCorsOriginAsync in ClientCorsOriginManager");
                throw new Exception("CreateClientCorsOriginApiModel is null , CreateClientCorsOriginAsync in ClientCorsOriginManager");
            }

            var createClientCorsOriginRepoParam = new CreateClientCorsOriginRepoParam(createClientCorsOriginApiModel);

            try
            {
                await _clientCorsOriginRepository.CreateClientCorsOriginAsync(createClientCorsOriginRepoParam);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task UpdateClientCorsOriginAsync(UpdateClientCorsOriginApiModel clientCorsOriginApiModel,int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null,UpdateClientCorsOriginAsync ClientCorsOriginManager");
                throw new Exception("id is null,UpdateClientCorsOriginAsync ClientCorsOriginManager");

            }
            
            if (clientCorsOriginApiModel == null)
            {
                _logger.LogError("in UpdateClientCorsOriginAsync ClientCorsOriginManager ,UpdateClientCorsOriginApiModel is null");
                throw new Exception("UpdateClientCorsOriginApiModel is null");

            }

            var updateClientCorsOriginRepoParam = new UpdateClientCorsOriginRepoParam(clientCorsOriginApiModel);
            
            try
            {
                await _clientCorsOriginRepository.UpdateClientCorsOriginAsync(updateClientCorsOriginRepoParam,id);
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task<List<ClientCorsOriginObject>> GetAllClientCorsOriginAsync()
        {
            
            try
            {
                var clientCorsOriginRepoResults = await _clientCorsOriginRepository.GetAllClientCorsOriginsAsync();
                
                var allClientCorsOriginObjects=new List<ClientCorsOriginObject>();
                
                foreach (var clientCorsOriginRepoResult in clientCorsOriginRepoResults)
                {
                    allClientCorsOriginObjects.Add(new ClientCorsOriginObject(clientCorsOriginRepoResult));
                }

                return allClientCorsOriginObjects;
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task<ClientCorsOriginObject> GetOneClientCorsOriginAsync(int? id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, GetOneClientCorsOriginAsync ClientCorsOriginManager");
                throw new Exception("id is null, GetOneClientCorsOriginAsync ClientCorsOriginManager");
            }

            try
            {
                var clientCorsOriginRepoResult = await _clientCorsOriginRepository.GetOneClientCorsOriginAsync(id);
                
                return new ClientCorsOriginObject(clientCorsOriginRepoResult);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }
        public async Task DeleteClientCorsOriginAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("Id is null,DeleteClientCorsOriginAsync ClientCorsOriginManager");
                throw new Exception("ID is null in DeleteClientCorsOriginAsync in ClientCorsOriginManager");
            }

            try
            {
                await _clientCorsOriginRepository.DeleteClientCorsOriginAsync(id);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
    }
}