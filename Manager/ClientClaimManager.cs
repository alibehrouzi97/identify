using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Models.ApiModels.ClientClaimApiModels;
using Identity.Models.Entities.ClientClaimEntity;
using Identity.Models.RepoParams.ClientClaim;
using Identity.Repository;
using Microsoft.Extensions.Logging;

namespace Identity.Manager
{
    public interface IClientClaimManager
    {
        Task UpdateClientClaimAsync(UpdateClientClaimApiModel clientClaimModel, int id);
        Task CreateClientClaimAsync(CreateClientClaimApiModel createClientClaimApiModel);
        Task<ClientClaimObject> GetOneClientClaimAsync(int? id);
        Task<List<ClientClaimObject>> GetAllClientClaimsAsync();
        Task DeleteClientClaimAsync(int id);
    }
    public class ClientClaimManager : IClientClaimManager
    {
        
        private readonly ILogger<ClientClaimManager> _logger;
        
        private readonly IClientClaimRepository _clientClaimRepository;
        
        public ClientClaimManager(ILogger<ClientClaimManager> logger, IClientClaimRepository clientClaimRepository)
        {
            _logger = logger;
            _clientClaimRepository = clientClaimRepository;
            
        }
        
        public async Task CreateClientClaimAsync(CreateClientClaimApiModel createClientClaimApiModel)
        {
            
            if (createClientClaimApiModel==null)
            {
                _logger.LogError("CreateClientClaimApiModel is null , CreateClientClaimAsync in ClientClaimManager");
                throw new Exception("CreateClientClaimApiModel is null , CreateClientClaimAsync in ClientClaimManager");
            }

            var createClientClaimRepoParam = new CreateClientClaimRepoParam(createClientClaimApiModel);

            try
            {
                await _clientClaimRepository.CreateClientClaimAsync(createClientClaimRepoParam);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task UpdateClientClaimAsync(UpdateClientClaimApiModel clientClaimModel,int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null,UpdateClientClaimAsync ClientClaimManager");
                throw new Exception("id is null,UpdateClientClaimAsync ClientClaimManager");

            }
            
            if (clientClaimModel == null)
            {
                _logger.LogError("in UpdateClientClaimAsync ClientClaimManager ,UpdateClientClaimApiModel is null");
                throw new Exception("UpdateClientApiModel is null");

            }

            var updateClientClaimRepoParam = new UpdateClientClaimRepoParam(clientClaimModel);
            
            try
            {
                await _clientClaimRepository.UpdateClientClaimAsync(updateClientClaimRepoParam,id);
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }
        public async Task<List<ClientClaimObject>> GetAllClientClaimsAsync()
        {
            
            try
            {
                var clientClaimRepoResults = await _clientClaimRepository.GetAllClientClaimsAsync();
                
                var allClientClaimObjects=new List<ClientClaimObject>();
                
                foreach (var clientClaimRepoResult in clientClaimRepoResults)
                {
                    allClientClaimObjects.Add(new ClientClaimObject(clientClaimRepoResult));
                }

                return allClientClaimObjects;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }
        
        public async Task<ClientClaimObject> GetOneClientClaimAsync(int? id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, GetOneClientClaimAsync ClientClaimManager");
                throw new Exception("id is null, GetOneClientClaimAsync ClientClaimManager");
            }

            try
            {
                var clientClaimRepoResult = await _clientClaimRepository.GetOneClientClaimAsync(id);
                
                return new ClientClaimObject(clientClaimRepoResult);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task DeleteClientClaimAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("Id is null,DeleteClientClaimAsync ClientClaimManager");
                throw new Exception("ID is null in DeleteClientClaimAsync in ClientClaimManager");
            }

            try
            {
                await _clientClaimRepository.DeleteClientClaimAsync(id);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        
    }
    
    
}