using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Models.ApiModels.ClientRedirectUriApiModels;
using Identity.Models.ApiModels.ClientScopeApiModels;
using Identity.Models.Entities.ClientRedirectUriEntity;
using Identity.Models.Entities.ClientScopeEntity;
using Identity.Models.RepoParams.ClientRedirectUriEntity;
using Identity.Models.RepoParams.ClientScopeEntity;
using Identity.Repository;
using Microsoft.Extensions.Logging;

namespace Identity.Manager
{
    public interface IClientScopeManager
    {
        Task DeleteClientScopeAsync(int id);
        Task<ClientScopeObject> GetOneClientScopeAsync(int? id);
        Task<List<ClientScopeObject>> GetAllClientScopesAsync();
        Task UpdateClientScopeAsync(UpdateClientScopeApiModel updateClientScopeApiModel, int id);
        Task CreateClientScopeAsync(CreateClientScopeApiModel createClientScopeApiModel);

    }
    public class ClientScopeManager : IClientScopeManager
    {
        
        private readonly ILogger<ClientRedirectUriManager> _logger;
        
        private readonly IClientScopeRepository _clientScopeRepository;
        
        public ClientScopeManager(ILogger<ClientRedirectUriManager> logger, IClientScopeRepository clientScopeRepository)
        {
            
            _logger = logger;
            _clientScopeRepository = clientScopeRepository;
        }
        
        public async Task CreateClientScopeAsync(CreateClientScopeApiModel createClientScopeApiModel)
        {
            
            if (createClientScopeApiModel==null)
            {
                _logger.LogError("createClientScopeApiModel is null , CreateClientScopeAsync in ClientScopeManager");
                throw new Exception("createClientScopeApiModel is null , CreateClientScopeAsync in ClientScopeManager");
            }

            var createClientScopeRepoParam = new CreateClientScopeRepoParam(createClientScopeApiModel);

            try
            {
                await _clientScopeRepository.CreateClientScopeAsync(createClientScopeRepoParam);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task UpdateClientScopeAsync(UpdateClientScopeApiModel updateClientScopeApiModel,int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null,UpdateClientScopeAsync ClientScopeManager");
                throw new Exception("id is null,UpdateClientScopeAsync ClientScopeManager");

            }
            
            if (updateClientScopeApiModel == null)
            {
                _logger.LogError("in UpdateClientScopeAsync ClientScopeManager ,updateClientScopeApiModel is null");
                throw new Exception("updateClientScopeApiModel is null");

            }

            var updateClientScopeRepoParam = new UpdateClientScopeRepoParam(updateClientScopeApiModel);
            
            try
            {
                await _clientScopeRepository.UpdateClientScopeAsync(updateClientScopeRepoParam,id);
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task<List<ClientScopeObject>> GetAllClientScopesAsync()
        {
            
            try
            {
                var createClientScopeRepoResults = await _clientScopeRepository.GetAllClientScopesAsync();
                
                var clientScopeObjects=new List<ClientScopeObject>();
                
                foreach (var clientScopeRepoResult in createClientScopeRepoResults)
                {
                    clientScopeObjects.Add(new ClientScopeObject(clientScopeRepoResult));
                }

                return clientScopeObjects;
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }
        public async Task<ClientScopeObject> GetOneClientScopeAsync(int? id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, GetOneClientScopeAsync ClientScopeManager");
                throw new Exception("id is null, GetOneClientScopeAsync ClientScopeManager");
            }

            try
            {
                var clientScopeRepoResult = await _clientScopeRepository.GetOneClientScopeAsync(id);
                return new ClientScopeObject(clientScopeRepoResult);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task DeleteClientScopeAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("Id is null,DeleteClientScopeAsync ClientScopeManager");
                throw new Exception("ID is null in DeleteClientScopeAsync in ClientScopeManager");
            }

            try
            {
                await _clientScopeRepository.DeleteClientScopeAsync(id);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }
        
    }
}