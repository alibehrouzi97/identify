using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Models.ApiModels.ApiResourcesModels;
using Identity.Models.Entities.ApiResourcesEntity;
using Identity.Models.RepoParams.ApiResources;
using Identity.Repository;
using Microsoft.Extensions.Logging;

namespace Identity.Manager
{
    public interface IApiResourcesManager
    {
        Task CreateApiResourcesAsync(CreateApiResourcesApiModel createApiResourcesApiModel);
        Task UpdateApiResourcesAsync(UpdateApiResourcesApiModel updateApiResourcesApiModel, int id);
        Task<List<ApiResourcesObject>> GetAllApiResourcesAsync();
        Task<ApiResourcesObject> GetOneApiResourcesAsync(int? id);
        Task DeleteApiResourcesAsync(int id);
    }
    public class ApiResourcesManager : IApiResourcesManager
    {
        
        private readonly ILogger<ApiResourcesManager> _logger;
        
        private readonly IApiResourcesRepository _apiResourcesRepository;
        
        public ApiResourcesManager(ILogger<ApiResourcesManager> logger, IApiResourcesRepository apiResourcesRepository)
        {
            _logger = logger;
            _apiResourcesRepository = apiResourcesRepository;
        }
        
        public async Task CreateApiResourcesAsync(CreateApiResourcesApiModel createApiResourcesApiModel)
        {
            
            if (createApiResourcesApiModel==null)
            {
                _logger.LogError("createApiResourcesApiModel is null , CreateApiResourcesAsync in ApiResourcesManager");
                throw new Exception("createApiResourcesApiModel is null , CreateApiResourcesAsync in ApiResourcesManager");
            }

            var createApiResourcesRepoParam = new CreateApiResourcesRepoParam(createApiResourcesApiModel);

            try
            {
                await _apiResourcesRepository.CreateApiResourcesAsync(createApiResourcesRepoParam);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task UpdateApiResourcesAsync(UpdateApiResourcesApiModel updateApiResourcesApiModel,int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null,UpdateApiResourcesAsync ApiResourcesManager");
                throw new Exception("id is null,UpdateApiResourcesAsync ApiResourcesManager");

            }
            
            if (updateApiResourcesApiModel == null)
            {
                _logger.LogError("in UpdateApiResourcesAsync ApiResourcesManager ,updateApiResourcesApiModel is null");
                throw new Exception("updateApiResourcesApiModel is null");

            }

            var updateApiResourcesRepoParam = new UpdateApiResourcesRepoParam(updateApiResourcesApiModel);
            
            try
            {
                await _apiResourcesRepository.UpdateApiResourcesAsync(updateApiResourcesRepoParam,id);
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task<List<ApiResourcesObject>> GetAllApiResourcesAsync()
        {
            
            try
            {
                
                var createApiResourcesRepoResults = await _apiResourcesRepository.GetAllApiResourcesAsync();
                
                var apiResourcesObjects=new List<ApiResourcesObject>();
                
                foreach (var createApiResourcesRepoResult in createApiResourcesRepoResults)
                {
                    apiResourcesObjects.Add(new ApiResourcesObject(createApiResourcesRepoResult));
                }

                return apiResourcesObjects;
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task<ApiResourcesObject> GetOneApiResourcesAsync(int? id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, GetOneApiResourcesAsync ApiResourcesManager");
                throw new Exception("id is null, GetOneApiResourcesAsync ApiResourcesManager");
            }

            try
            {
                
                var apiResourcesRepoResult = await _apiResourcesRepository.GetOneApiResourcesAsync(id);
                
                return new ApiResourcesObject(apiResourcesRepoResult);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task DeleteApiResourcesAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("Id is null,DeleteApiResourcesAsync ApiResourcesManager");
                throw new Exception("ID is null in DeleteApiResourcesAsync in ApiResourcesManager");
            }

            try
            {
                await _apiResourcesRepository.DeleteApiResourcesAsync(id);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }
        
    }
}