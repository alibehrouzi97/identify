using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Models.ApiModels.ClientApiModels;
using Identity.Models.Entities.ClientEntity;
using Identity.Models.RepoParams.Client;
using Identity.Repository;
using Microsoft.Extensions.Logging;

namespace Identity.Manager
{
    public interface IClientManager
    {
        Task CreateClientAsync(CreateClientApiModel createClientApiModel);
        Task<List<ClientObject>> GetAllClientsAsync();
        Task<ClientObject> GetOneClientAsync(int? id);
        Task DeleteClientAsync(int id);
        Task UpdateClientAsync(UpdateClientApiModel clientModel, int id);
    }
    public class ClientManager : IClientManager
    {
        
        private readonly ILogger<ClientManager> _logger;
        
        private readonly IClientRepository _clientRepository;

        public ClientManager(ILogger<ClientManager> logger, IClientRepository clientRepository)
        {
            
            _logger = logger;
            _clientRepository = clientRepository;
            
        }

        public async Task CreateClientAsync(CreateClientApiModel createClientApiModel)
        {
            
            if (createClientApiModel==null)
            {
                _logger.LogError("CreateClientApiModel is null , CreateClientAsync in ClientManager");
                throw new Exception("CreateClientApiModel is null , CreateClientAsync in ClientManager");
            }

            var createClientRepoParam = new CreateClientRepoParam(createClientApiModel);

            try
            {
                await _clientRepository.CreateClientAsync(createClientRepoParam);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        
        public async Task UpdateClientAsync(UpdateClientApiModel clientModel,int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null,UpdateClientAsync ClientManager");
                throw new Exception("id is null,UpdateClientAsync ClientManager");

            }
            
            if (clientModel == null)
            {
                _logger.LogError("in UpdateClientAsync ClientManager ,UpdateClientApiModel is null");
                throw new Exception("UpdateClientApiModel is null");

            }

            var updateClientRepoParam = new UpdateClientRepoParam(clientModel);
            
            try
            {
                await _clientRepository.UpdateClientAsync(updateClientRepoParam,id);
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }

        }

        public async Task<List<ClientObject>> GetAllClientsAsync()
        {
            
            try
            {
                
                var clientRepoResults = await _clientRepository.GetAllClientsAsync();
                
                var allClientObjects=new List<ClientObject>();
                
                foreach (var clientRepoResult in clientRepoResults)
                {
                    allClientObjects.Add(new ClientObject(clientRepoResult));
                }

                return allClientObjects;
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }

        public async Task<ClientObject> GetOneClientAsync(int? id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, GetOneClientAsync ClientManager");
                throw new Exception("id is null, GetOneClientAsync ClientManager");
            }

            try
            {
                var clientRepoResult = await _clientRepository.GetOneClientAsync(id);
                return new ClientObject(clientRepoResult);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }
        
        public async Task DeleteClientAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("Id is null,DeleteClientAsync ClientManager");
                throw new Exception("ID is null in DeleteClientAsync in ClientManager");
            }

            try
            {
                await _clientRepository.DeleteClientAsync(id);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }
    }
}