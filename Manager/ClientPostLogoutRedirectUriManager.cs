using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Models.ApiModels.ClientGrantTypeApiModels;
using Identity.Models.ApiModels.ClientPostLogoutRedirectUriApiModuels;
using Identity.Models.Entities.ClientGrantTypeEntity;
using Identity.Models.Entities.ClientPostLogoutRedirectUriEntity;
using Identity.Models.RepoParams.ClientGrantType;
using Identity.Models.RepoParams.ClientPostLogoutRedirectUri;
using Identity.Repository;
using Microsoft.Extensions.Logging;

namespace Identity.Manager
{
    public interface IClientPostLogoutRedirectUriManager
    {
        Task CreateClientPostLogoutRedirectUriAsync(
            CreateClientPostLogoutRedirectUriApiModel createClientPostLogoutRedirectUriApiModel);

        Task UpdateClientPostLogoutRedirectUriAsync(
            UpdateClientPostLogoutRedirectUriApiModel updateClientPostLogoutRedirectUriApiModel, int id);

        Task<List<ClientPostLogoutRedirectUriObject>> GetAllClientPostLogoutRedirectUrisAsync();
        Task<ClientPostLogoutRedirectUriObject> GetOneClientPostLogoutRedirectUriAsync(int? id);
        Task DeleteClientPostLogoutRedirectUriAsync(int id);

    }
    public class ClientPostLogoutRedirectUriManager : IClientPostLogoutRedirectUriManager
    {
        
        private readonly ILogger<ClientPostLogoutRedirectUriManager> _logger;
        
        private readonly IClientPostLogoutRedirectUriRepository _clientPostLogoutRedirectUriRepository;
        
        public ClientPostLogoutRedirectUriManager(ILogger<ClientPostLogoutRedirectUriManager> logger, IClientPostLogoutRedirectUriRepository clientPostLogoutRedirectUriRepository)
        {
            _logger = logger;
            _clientPostLogoutRedirectUriRepository = clientPostLogoutRedirectUriRepository;
        }
        
        public async Task CreateClientPostLogoutRedirectUriAsync(CreateClientPostLogoutRedirectUriApiModel createClientPostLogoutRedirectUriApiModel)
        {
            
            if (createClientPostLogoutRedirectUriApiModel==null)
            {
                _logger.LogError("CreateClientPostLogoutRedirectUriApiModel is null , CreateClientPostLogoutRedirectUriAsync in ClientPostLogoutRedirectUriManager");
                throw new Exception("CreateClientPostLogoutRedirectUriApiModel is null , CreateClientPostLogoutRedirectUriAsync in ClientPostLogoutRedirectUriManager");
            }

            var createClientPostLogoutRedirectUriRepoParam = new CreateClientPostLogoutRedirectUriRepoParam(createClientPostLogoutRedirectUriApiModel);

            try
            {
                await _clientPostLogoutRedirectUriRepository.CreateClientPostLogoutRedirectUriAsync(createClientPostLogoutRedirectUriRepoParam);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task UpdateClientPostLogoutRedirectUriAsync(UpdateClientPostLogoutRedirectUriApiModel updateClientPostLogoutRedirectUriApiModel,int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null,UpdateClientPostLogoutRedirectUriAsync ClientPostLogoutRedirectUriManager");
                throw new Exception("id is null,UpdateClientPostLogoutRedirectUriAsync ClientPostLogoutRedirectUriManager");

            }
            
            if (updateClientPostLogoutRedirectUriApiModel == null)
            {
                _logger.LogError("in UpdateClientPostLogoutRedirectUriAsync ClientPostLogoutRedirectUriManager ,UpdateClientPostLogoutRedirectUriApiModel is null");
                throw new Exception("updateClientPostLogoutRedirectUriApiModel is null");

            }
            

            var updateClientPostLogoutRedirectUriRepoParam = new UpdateClientPostLogoutRedirectUriRepoParam(updateClientPostLogoutRedirectUriApiModel);
            
            try
            {
                await _clientPostLogoutRedirectUriRepository.UpdateClientPostLogoutRedirectUriAsync(updateClientPostLogoutRedirectUriRepoParam,id);
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task<List<ClientPostLogoutRedirectUriObject>> GetAllClientPostLogoutRedirectUrisAsync()
        {
            
            try
            {
                var clientPostLogoutRedirectUriRepoResults = await _clientPostLogoutRedirectUriRepository.GetAllClientPostLogoutRedirectUrisAsync();
                
                var clientPostLogoutRedirectUriObjects=new List<ClientPostLogoutRedirectUriObject>();
                
                foreach (var clientPostLogoutRedirectUriRepoResult in clientPostLogoutRedirectUriRepoResults)
                {
                    clientPostLogoutRedirectUriObjects.Add(new ClientPostLogoutRedirectUriObject(clientPostLogoutRedirectUriRepoResult));
                }

                return clientPostLogoutRedirectUriObjects;
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }
        public async Task<ClientPostLogoutRedirectUriObject> GetOneClientPostLogoutRedirectUriAsync(int? id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, GetOneClientPostLogoutRedirectUriAsync ClientPostLogoutRedirectUriManager");
                throw new Exception("id is null, GetOneClientPostLogoutRedirectUriAsync ClientPostLogoutRedirectUriManager");
            }

            try
            {
                var clientPostLogoutRedirectUriRepoResult = await _clientPostLogoutRedirectUriRepository.GetOneClientPostLogoutRedirectUriAsync(id);
                
                return new ClientPostLogoutRedirectUriObject(clientPostLogoutRedirectUriRepoResult);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task DeleteClientPostLogoutRedirectUriAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("Id is null,DeleteClientPostLogoutRedirectUriAsync ClientPostLogoutRedirectUriManager");
                throw new Exception("ID is null in DeleteClientPostLogoutRedirectUriAsync in ClientPostLogoutRedirectUriManager");
            }

            try
            {
                await _clientPostLogoutRedirectUriRepository.DeleteClientPostLogoutRedirectUriAsync(id);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
    }
}