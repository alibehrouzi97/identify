using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Models.ApiModels.ClientPostLogoutRedirectUriApiModuels;
using Identity.Models.ApiModels.ClientRedirectUriApiModels;
using Identity.Models.Entities.ClientPostLogoutRedirectUriEntity;
using Identity.Models.Entities.ClientRedirectUriEntity;
using Identity.Models.RepoParams.ClientPostLogoutRedirectUri;
using Identity.Models.RepoParams.ClientRedirectUriEntity;
using Identity.Repository;
using Microsoft.Extensions.Logging;

namespace Identity.Manager
{
    public interface IClientRedirectUriManager
    {
        Task DeleteClientRedirectUriAsync(int id);
        Task<ClientRedirectUriObject> GetOneClientRedirectUriAsync(int? id);
        Task<List<ClientRedirectUriObject>> GetAllClientRedirectUrisAsync();
        Task UpdateClientRedirectUriAsync(UpdateClientRedirectUriApiModel updateClientRedirectUriApiModel, int id);
        Task CreateClientRedirectUriAsync(CreateClientRedirectUriApiModel createClientRedirectUriApiModel);

    }
    public class ClientRedirectUriManager : IClientRedirectUriManager
    {
        
        private readonly ILogger<ClientRedirectUriManager> _logger;
        
        private readonly IClientRedirectUriRepository _clientRedirectUriRepository;
        
        public ClientRedirectUriManager(ILogger<ClientRedirectUriManager> logger, IClientRedirectUriRepository clientRedirectUriRepository)
        {
            _logger = logger;
            _clientRedirectUriRepository = clientRedirectUriRepository;
        }
        
         public async Task CreateClientRedirectUriAsync(CreateClientRedirectUriApiModel createClientRedirectUriApiModel)
        {
            
            if (createClientRedirectUriApiModel==null)
            {
                _logger.LogError("createClientRedirectUriApiModel is null , CreateClientRedirectUriAsync in ClientRedirectUriManager");
                throw new Exception("createClientRedirectUriApiModel is null , CreateClientRedirectUriAsync in ClientRedirectUriManager");
            }

            var createClientRedirectUriRepoParam = new CreateClientRedirectUriRepoParam(createClientRedirectUriApiModel);

            try
            {
                await _clientRedirectUriRepository.CreateClientRedirectUriAsync(createClientRedirectUriRepoParam);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }
         
        public async Task UpdateClientRedirectUriAsync(UpdateClientRedirectUriApiModel updateClientRedirectUriApiModel,int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null,UpdateClientRedirectUriAsync ClientRedirectUriManager");
                throw new Exception("id is null,UpdateClientRedirectUriAsync ClientRedirectUriManager");

            }
            
            if (updateClientRedirectUriApiModel == null)
            {
                _logger.LogError("in UpdateClientRedirectUriAsync ClientRedirectUriManager ,updateClientRedirectUriApiModel is null");
                throw new Exception("updateClientRedirectUriApiModel is null");

            }

            var updateClientRedirectUriRepoParam = new UpdateClientRedirectUriRepoParam(updateClientRedirectUriApiModel);
            
            try
            {
                await _clientRedirectUriRepository.UpdateClientRedirectUriAsync(updateClientRedirectUriRepoParam,id);
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task<List<ClientRedirectUriObject>> GetAllClientRedirectUrisAsync()
        {
            
            try
            {
                var createClientRedirectUriRepoResults = await _clientRedirectUriRepository.GetAllClientRedirectUrisAsync();
                
                var redirectUriObjects=new List<ClientRedirectUriObject>();
                
                foreach (var createClientRedirectUriRepoResult in createClientRedirectUriRepoResults)
                {
                    redirectUriObjects.Add(new ClientRedirectUriObject(createClientRedirectUriRepoResult));
                }

                return redirectUriObjects;
                
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
        }
        public async Task<ClientRedirectUriObject> GetOneClientRedirectUriAsync(int? id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("id is null, GetOneClientRedirectUriAsync ClientRedirectUriManager");
                throw new Exception("id is null, GetOneClientRedirectUriAsync ClientRedirectUriManager");
            }

            try
            {
                var clientRedirectUriRepoResult = await _clientRedirectUriRepository.GetOneClientRedirectUriAsync(id);
                return new ClientRedirectUriObject(clientRedirectUriRepoResult);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }
        public async Task DeleteClientRedirectUriAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                _logger.LogError("Id is null,DeleteClientRedirectUriAsync ClientRedirectUriManager");
                throw new Exception("ID is null in DeleteClientRedirectUriAsync in ClientRedirectUriManager");
            }

            try
            {
                await _clientRedirectUriRepository.DeleteClientRedirectUriAsync(id);
            }
            
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }
    }
}