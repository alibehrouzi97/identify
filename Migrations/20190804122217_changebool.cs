﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Identity.Migrations
{
    public partial class changebool : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClientClaim_Client_ClientId",
                table: "ClientClaim");

            migrationBuilder.DropForeignKey(
                name: "FK_ClientCorsOrigin_Client_ClientId",
                table: "ClientCorsOrigin");

            migrationBuilder.DropForeignKey(
                name: "FK_ClientGrantType_Client_ClientId",
                table: "ClientGrantType");

            migrationBuilder.DropForeignKey(
                name: "FK_ClientPostLogoutRedirectUri_Client_ClientId",
                table: "ClientPostLogoutRedirectUri");

            migrationBuilder.DropForeignKey(
                name: "FK_ClientRedirectUri_Client_ClientId",
                table: "ClientRedirectUri");

            migrationBuilder.DropForeignKey(
                name: "FK_ClientScope_Client_ClientId",
                table: "ClientScope");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClientScope",
                table: "ClientScope");

            migrationBuilder.DropIndex(
                name: "IX_ClientScope_ClientId",
                table: "ClientScope");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClientRedirectUri",
                table: "ClientRedirectUri");

            migrationBuilder.DropIndex(
                name: "IX_ClientRedirectUri_ClientId",
                table: "ClientRedirectUri");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClientPostLogoutRedirectUri",
                table: "ClientPostLogoutRedirectUri");

            migrationBuilder.DropIndex(
                name: "IX_ClientPostLogoutRedirectUri_ClientId",
                table: "ClientPostLogoutRedirectUri");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClientGrantType",
                table: "ClientGrantType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClientCorsOrigin",
                table: "ClientCorsOrigin");

            migrationBuilder.DropIndex(
                name: "IX_ClientCorsOrigin_ClientId",
                table: "ClientCorsOrigin");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClientClaim",
                table: "ClientClaim");

            migrationBuilder.DropIndex(
                name: "IX_ClientClaim_ClientId",
                table: "ClientClaim");

            migrationBuilder.RenameTable(
                name: "ClientScope",
                newName: "ClientScopes");

            migrationBuilder.RenameTable(
                name: "ClientRedirectUri",
                newName: "ClientRedirectUris");

            migrationBuilder.RenameTable(
                name: "ClientPostLogoutRedirectUri",
                newName: "ClientPostLogoutRedirectUris");

            migrationBuilder.RenameTable(
                name: "ClientGrantType",
                newName: "ClientGrantTypes");

            migrationBuilder.RenameTable(
                name: "ClientCorsOrigin",
                newName: "ClientCorsOrigins");

            migrationBuilder.RenameTable(
                name: "ClientClaim",
                newName: "ClientClaims");

            migrationBuilder.RenameIndex(
                name: "IX_ClientGrantType_ClientId",
                table: "ClientGrantTypes",
                newName: "IX_ClientGrantTypes_ClientId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClientScopes",
                table: "ClientScopes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClientRedirectUris",
                table: "ClientRedirectUris",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClientPostLogoutRedirectUris",
                table: "ClientPostLogoutRedirectUris",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClientGrantTypes",
                table: "ClientGrantTypes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClientCorsOrigins",
                table: "ClientCorsOrigins",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClientClaims",
                table: "ClientClaims",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ClientGrantTypes_Client_ClientId",
                table: "ClientGrantTypes",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClientGrantTypes_Client_ClientId",
                table: "ClientGrantTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClientScopes",
                table: "ClientScopes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClientRedirectUris",
                table: "ClientRedirectUris");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClientPostLogoutRedirectUris",
                table: "ClientPostLogoutRedirectUris");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClientGrantTypes",
                table: "ClientGrantTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClientCorsOrigins",
                table: "ClientCorsOrigins");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClientClaims",
                table: "ClientClaims");

            migrationBuilder.RenameTable(
                name: "ClientScopes",
                newName: "ClientScope");

            migrationBuilder.RenameTable(
                name: "ClientRedirectUris",
                newName: "ClientRedirectUri");

            migrationBuilder.RenameTable(
                name: "ClientPostLogoutRedirectUris",
                newName: "ClientPostLogoutRedirectUri");

            migrationBuilder.RenameTable(
                name: "ClientGrantTypes",
                newName: "ClientGrantType");

            migrationBuilder.RenameTable(
                name: "ClientCorsOrigins",
                newName: "ClientCorsOrigin");

            migrationBuilder.RenameTable(
                name: "ClientClaims",
                newName: "ClientClaim");

            migrationBuilder.RenameIndex(
                name: "IX_ClientGrantTypes_ClientId",
                table: "ClientGrantType",
                newName: "IX_ClientGrantType_ClientId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClientScope",
                table: "ClientScope",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClientRedirectUri",
                table: "ClientRedirectUri",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClientPostLogoutRedirectUri",
                table: "ClientPostLogoutRedirectUri",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClientGrantType",
                table: "ClientGrantType",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClientCorsOrigin",
                table: "ClientCorsOrigin",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClientClaim",
                table: "ClientClaim",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_ClientScope_ClientId",
                table: "ClientScope",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientRedirectUri_ClientId",
                table: "ClientRedirectUri",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientPostLogoutRedirectUri_ClientId",
                table: "ClientPostLogoutRedirectUri",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientCorsOrigin_ClientId",
                table: "ClientCorsOrigin",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientClaim_ClientId",
                table: "ClientClaim",
                column: "ClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_ClientClaim_Client_ClientId",
                table: "ClientClaim",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClientCorsOrigin_Client_ClientId",
                table: "ClientCorsOrigin",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClientGrantType_Client_ClientId",
                table: "ClientGrantType",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClientPostLogoutRedirectUri_Client_ClientId",
                table: "ClientPostLogoutRedirectUri",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClientRedirectUri_Client_ClientId",
                table: "ClientRedirectUri",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClientScope_Client_ClientId",
                table: "ClientScope",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
