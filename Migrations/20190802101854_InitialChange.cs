﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Identity.Migrations
{
    public partial class InitialChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "id",
                table: "ClientScope",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "ClientRedirectUri",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "ClientPostLogoutRedirectUri",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "ClientCorsOrigin",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "value",
                table: "ClientClaim",
                newName: "Value");

            migrationBuilder.RenameColumn(
                name: "type",
                table: "ClientClaim",
                newName: "Type");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "ClientClaim",
                newName: "Id");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Client",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ClientName",
                table: "Client",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                table: "ClientScope",
                newName: "id");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "ClientRedirectUri",
                newName: "id");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "ClientPostLogoutRedirectUri",
                newName: "id");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "ClientCorsOrigin",
                newName: "id");

            migrationBuilder.RenameColumn(
                name: "Value",
                table: "ClientClaim",
                newName: "value");

            migrationBuilder.RenameColumn(
                name: "Type",
                table: "ClientClaim",
                newName: "type");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "ClientClaim",
                newName: "id");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Client",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "ClientName",
                table: "Client",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
