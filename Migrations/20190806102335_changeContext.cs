﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Identity.Migrations
{
    public partial class changeContext : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_ClientScopes_ClientId",
                table: "ClientScopes",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientRedirectUris_ClientId",
                table: "ClientRedirectUris",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientPostLogoutRedirectUris_ClientId",
                table: "ClientPostLogoutRedirectUris",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientCorsOrigins_ClientId",
                table: "ClientCorsOrigins",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientClaims_ClientId",
                table: "ClientClaims",
                column: "ClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_ClientClaims_Client_ClientId",
                table: "ClientClaims",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClientCorsOrigins_Client_ClientId",
                table: "ClientCorsOrigins",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClientPostLogoutRedirectUris_Client_ClientId",
                table: "ClientPostLogoutRedirectUris",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClientRedirectUris_Client_ClientId",
                table: "ClientRedirectUris",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClientScopes_Client_ClientId",
                table: "ClientScopes",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClientClaims_Client_ClientId",
                table: "ClientClaims");

            migrationBuilder.DropForeignKey(
                name: "FK_ClientCorsOrigins_Client_ClientId",
                table: "ClientCorsOrigins");

            migrationBuilder.DropForeignKey(
                name: "FK_ClientPostLogoutRedirectUris_Client_ClientId",
                table: "ClientPostLogoutRedirectUris");

            migrationBuilder.DropForeignKey(
                name: "FK_ClientRedirectUris_Client_ClientId",
                table: "ClientRedirectUris");

            migrationBuilder.DropForeignKey(
                name: "FK_ClientScopes_Client_ClientId",
                table: "ClientScopes");

            migrationBuilder.DropIndex(
                name: "IX_ClientScopes_ClientId",
                table: "ClientScopes");

            migrationBuilder.DropIndex(
                name: "IX_ClientRedirectUris_ClientId",
                table: "ClientRedirectUris");

            migrationBuilder.DropIndex(
                name: "IX_ClientPostLogoutRedirectUris_ClientId",
                table: "ClientPostLogoutRedirectUris");

            migrationBuilder.DropIndex(
                name: "IX_ClientCorsOrigins_ClientId",
                table: "ClientCorsOrigins");

            migrationBuilder.DropIndex(
                name: "IX_ClientClaims_ClientId",
                table: "ClientClaims");
        }
    }
}
