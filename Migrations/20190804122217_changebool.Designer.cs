﻿// <auto-generated />
using System;
using Identity.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Identity.Migrations
{
    [DbContext(typeof(MvcIdentityDbContext))]
    [Migration("20190804122217_changebool")]
    partial class changebool
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("Identity.Models.Entities.ClientClaimEntity.ClientClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ClientId");

                    b.Property<string>("Type");

                    b.Property<string>("Value");

                    b.HasKey("Id");

                    b.ToTable("ClientClaims");
                });

            modelBuilder.Entity("Identity.Models.Entities.ClientCorsOriginEntity.ClientCorsOrigin", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ClientId");

                    b.Property<string>("Origin");

                    b.HasKey("Id");

                    b.ToTable("ClientCorsOrigins");
                });

            modelBuilder.Entity("Identity.Models.Entities.ClientEntity.Client", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("AllowOfflineAccess");

                    b.Property<bool>("AlwaysSendClientClaims");

                    b.Property<string>("ClientName")
                        .IsRequired();

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<DateTime?>("LastAccessed");

                    b.Property<bool>("RequirePkce");

                    b.HasKey("Id");

                    b.ToTable("Client");
                });

            modelBuilder.Entity("Identity.Models.Entities.ClientGrantTypeEntity.ClientGrantType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ClientId");

                    b.Property<string>("GrantType");

                    b.HasKey("Id");

                    b.HasIndex("ClientId");

                    b.ToTable("ClientGrantTypes");
                });

            modelBuilder.Entity("Identity.Models.Entities.ClientPostLogoutRedirectUriEntity.ClientPostLogoutRedirectUri", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ClientId");

                    b.Property<string>("PostLogoutRedirectUri");

                    b.HasKey("Id");

                    b.ToTable("ClientPostLogoutRedirectUris");
                });

            modelBuilder.Entity("Identity.Models.Entities.ClientRedirectUriEntity.ClientRedirectUri", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ClientId");

                    b.Property<string>("RedirectUri");

                    b.HasKey("Id");

                    b.ToTable("ClientRedirectUris");
                });

            modelBuilder.Entity("Identity.Models.Entities.ClientScopeEntity.ClientScope", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ClientId");

                    b.Property<string>("Scope");

                    b.HasKey("Id");

                    b.ToTable("ClientScopes");
                });

            modelBuilder.Entity("Identity.Models.Entities.ClientGrantTypeEntity.ClientGrantType", b =>
                {
                    b.HasOne("Identity.Models.Entities.ClientEntity.Client")
                        .WithMany("AllowedGrantTypes")
                        .HasForeignKey("ClientId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
